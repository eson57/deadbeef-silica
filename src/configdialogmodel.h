#ifndef __CONFIGDIALOGMODEL_H
#define __CONFIGDIALOGMODEL_H

#include "configschemeparser.h"

#include <deadbeef/deadbeef.h>

#include <QAbstractListModel>

class CConfigDialogModel: public QAbstractListModel, IConfigSchemeConsumer
{
    Q_OBJECT

    Q_PROPERTY(QString ptr READ ptr WRITE setPtr NOTIFY ptrChanged)
    Q_PROPERTY(QString pluginName READ pluginName NOTIFY pluginNameChanged)

public:
    enum EConfigDialogModelRoles
    {
        LabelRole = Qt::UserRole,
        TypeRole,
        KeyRole,
        InitialValueRole,
        ValueRole,
        ScaleMinRole,
        ScaleMaxRole,
        ScaleStepRole,
        SelectCountRole,
        SelectItemsRole
    };

    enum EPropertyType
    {
        EntryType,
        PasswordType,
        FileType,
        CheckboxType,
        HscaleType,
        VscaleType,
        SelectType,
        UnknownType
    };

    Q_ENUM(EPropertyType)

    CConfigDialogModel(DB_functions_t *api);
    ~CConfigDialogModel();

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

    Q_INVOKABLE bool setValue(int row, const QVariant &value);

    QString ptr();
    void setPtr(QString ptr);

    QString pluginName();

    Q_INVOKABLE void commitChanges();
    Q_INVOKABLE void discardChanges();

    // scheme comsumer methods
    virtual void OnSchemeBegin();

    virtual void OnPropertyBegin();

    virtual void OnLabel(QString label);
    virtual void OnType(EConfigPropertyType type);
    virtual void OnKey(QString key);
    virtual void OnScaleMin(float min);
    virtual void OnScaleMax(float max);
    virtual void OnScaleStep(float step);
    virtual void OnSelectCount(int count);
    virtual void OnStringDefaultValue(QString defaultValue);
    virtual void OnIntDefaultValue(int defaultValue);
    virtual void OnBoolDefaultValue(bool defaultValue);
    virtual void OnFloatDefaultValue(float defaultValue);

    virtual void OnSelectItem(QString item);

    virtual void OnPropertyEnd();

    virtual void OnSchemeEnd();

    virtual void OnError();

signals:
    void ptrChanged();
    void pluginNameChanged();

protected:
    QHash<int, QByteArray> roleNames() const;

private:
    struct SDialogProperty
    {
        QString label;
        EPropertyType type;
        QString key;
        QVariant initialValue;
        QVariant value;

        union
        {
            struct
            {
                float min;
                float max;
                float step;
            } scaleParams;

            int selectCount;
        };

        QStringList selectItems;
    };

    QVariant GetIntProperty(QString key, int defaultValue);
    QVariant GetBoolProperty(QString key, bool defaultValue);
    QVariant GetStringProperty(QString key, QString defaultValue);
    QVariant GetFloatProperty(QString key, float defaultValue);
    void SetProperty(QString key, QVariant value);

    DB_functions_t *m_api;

    QString m_ptr;
    QVector<SDialogProperty> m_properties;

    QString m_pluginName;

    // Parsing accumulator
    SDialogProperty m_accProperty;
};

#endif // __CONFIGDIALOGMODEL_H
