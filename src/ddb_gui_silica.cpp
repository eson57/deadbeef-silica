#include "ddbapi.h"
#include "filesystemmodel.h"
#include "fs.h"
#include "version.h"
#include "logger.h"

#include <deadbeef/deadbeef.h>

#include <QScopedPointer>
#include <QGuiApplication>
#include <QQuickView>
#include <QFileInfo>
#include <QQmlEngine>
#include <QProcessEnvironment>
#include <QTranslator>

#include <stdio.h>
#include <unistd.h>
#include <stdint.h>
#include <assert.h>

typedef struct
{
    DB_gui_t gui;
} ddb_gui_silica_t;

static const char applicationName[] = "deadbeef";

static ddb_gui_silica_t s_gui_silica;

// FIXME: Do we need volatile here?
static QGuiApplication *s_app;
static CDdbApi *s_ddbApi;

static QGuiApplication* sailorizeApplication(QGuiApplication *app)
{
    LOG_DBG("Sailorizing app with name %s", applicationName);

    app->setOrganizationName(applicationName);
    app->setOrganizationDomain(applicationName);
    app->setApplicationName(applicationName);

    return app;
}

static QObject* GetDdbApiInstance(QQmlEngine *qmlEngine, QJSEngine *jsEngine)
{
    Q_UNUSED(qmlEngine);
    Q_UNUSED(jsEngine);

    assert(s_ddbApi);

    return s_ddbApi;
}

static QObject* GetFsInstance(QQmlEngine *qmlEngine, QJSEngine *jsEngine)
{
    Q_UNUSED(qmlEngine);
    Q_UNUSED(jsEngine);

    static QObject *fs = NULL;

    if (fs == NULL)
        fs = new CFs();

    return fs;
}

static QString GetLocalTranslationsDir()
{
    QProcessEnvironment env = QProcessEnvironment::systemEnvironment();

    QString xdgDataHome = env.value("XDG_DATA_HOME");
    QString home = env.value("HOME");

    LOG_DBG("XDG_DATA_HOME: %s", xdgDataHome.toUtf8().constData());
    LOG_DBG("HOME: %s", home.toUtf8().constData());

    QString localTranslationsDir;

    if (!xdgDataHome.isEmpty())
    {
        localTranslationsDir = QString("%1/deadbeef/translations").arg(xdgDataHome);
    }
    else if (!home.isEmpty())
    {
        localTranslationsDir = QString("%1/.local/share/deadbeef/translations").arg(home);
    }
    else
    {
        LOG_ERR("Failed to determine local translations directory: both XDG_DATA_HOME and HOME are not set");
    }

    return localTranslationsDir;
}

static QString GetSystemTranslationsDir(QString installPrefix)
{
    if (installPrefix.isEmpty())
        return "";

    return QString("%1/share/deadbeef/translations").arg(installPrefix);
}

static void InstallTranslation(QGuiApplication *app)
{
    QLocale locale;

    QTranslator *translator = new QTranslator(app);

    QString localTranslationsDir = GetLocalTranslationsDir();
    QString systemTranslationsDir = GetSystemTranslationsDir(s_ddbApi->getInstallPrefix());

    LOG_DBG("Local translations directory is '%s'", localTranslationsDir.toUtf8().constData());
    LOG_DBG("System translations directory is '%s'", systemTranslationsDir.toUtf8().constData());

    if (!localTranslationsDir.isEmpty() &&
        translator->load(locale, "deadbeef-silica", "-", localTranslationsDir))
    {
        LOG_DBG("Installing translation for %s from local translations dir", locale.name().toUtf8().data());
        app->installTranslator(translator);
    }
    else if (!systemTranslationsDir.isEmpty() &&
             translator->load(locale, "deadbeef-silica", "-", systemTranslationsDir))
    {
        LOG_DBG("Installing translation for %s from system translations dir", locale.name().toUtf8().data());
        app->installTranslator(translator);
    }
    else if (translator->load(locale, "deadbeef-silica", "-", ":/translations/"))
    {
        LOG_DBG("Installing translation for %s from resources", locale.name().toUtf8().data());
        app->installTranslator(translator);
    }
    else
    {
        LOG_DBG("Translation for %s was not found", locale.name().toUtf8().data());
        delete translator;
    }
}

static int ddb_gui_silica_start()
{
    LOG_DBG("Starting plugin");

    Q_INIT_RESOURCE(deadbeef_silica);

    QByteArray executablePath(applicationName);

    // We have to hardcode this since we do not have access to the real argc/argv
    int argc = 1;
    char* argv[] = { executablePath.data(), NULL };

    QScopedPointer<QGuiApplication> app(sailorizeApplication(new QGuiApplication(argc, argv)));

    if (s_ddbApi->confGetInt("silica.translations.disable") == 0)
        InstallTranslation(app.data());

    QScopedPointer<QQuickView> view(new QQuickView());

    // Allow access to app from ddb_gui_silica_stop()
    s_app = app.data();

    // register types
    qmlRegisterSingletonType<CDdbApi>("deadbeef", 1, 0, "DdbApi", GetDdbApiInstance);
    qmlRegisterType<CFileSystemModel>("deadbeef", 1, 0, "FileSystemModel");
    qmlRegisterSingletonType<CFs>("deadbeef", 1, 0, "Fs", GetFsInstance);

    app->setQuitOnLastWindowClosed(false);

    view->setTitle("DeadBeef");
    view->setSource(QUrl("qrc:/qml/ddb_gui_silica.qml"));
    view->show();

    QObject::connect(view->engine(), &QQmlEngine::quit, app.data(), &QGuiApplication::quit);
    QObject::connect(app.data(), &QGuiApplication::lastWindowClosed, s_ddbApi, &CDdbApi::terminate);

    LOG_DBG("Entering event loop");

    app->exec();

    Q_CLEANUP_RESOURCE(deadbeef_silica);

    LOG_DBG("Plugin returns control");

    return 0;
}

static int ddb_gui_silica_stop()
{
    static bool once = true;

    if (once)
    {
        LOG_DBG("Requested plugin shutdown");
        once = false;
        QMetaObject::invokeMethod(s_app, "quit");
    }

    return 0;
}

static int ddb_gui_silica_connect()
{
    assert(s_ddbApi);

    return s_ddbApi->onConnect();
}

static int ddb_gui_silica_message(uint32_t id, uintptr_t ctx, uint32_t p1, uint32_t p2)
{
    assert(s_ddbApi);

    return s_ddbApi->onDdbMessage(id, ctx, p1, p2);
}

static void InitializeLogger(DB_functions_t *api)
{
    api->conf_lock();

    QString severityStr = QString::fromUtf8(api->conf_get_str_fast("silica.logging.severity",
                                                                   "error"));

    api->conf_unlock();

    ELoggerSeverity severity = severityStr == "debug"   ? LoggerSeverityDebug :
                               severityStr == "warning" ? LoggerSeverityWarning :
                                                          LoggerSeverityError;

    LoggerInit(severity);
}

extern "C" Q_DECL_EXPORT DB_plugin_t* ddb_gui_silica_load(DB_functions_t *api)
{
    InitializeLogger(api);

    LOG_DBG("Initializing Deadbeef-Silica %d.%d", VERSION_MAJOR, VERSION_MINOR);

    // The only way to make sure that deadbeef will not call ddb_gui_silica_message()
    // in separate thread until we create CDdbApi instance is to create this instance
    // before we return our DB_plugin_t*
    s_ddbApi = new CDdbApi(api);

    s_gui_silica.gui.plugin.type = DB_PLUGIN_GUI;

    // Specify minimal API version
    s_gui_silica.gui.plugin.api_vmajor = 1;
    s_gui_silica.gui.plugin.api_vminor = 8;

    s_gui_silica.gui.plugin.version_major = VERSION_MAJOR;
    s_gui_silica.gui.plugin.version_minor = VERSION_MINOR;

    s_gui_silica.gui.plugin.id = "ddb_gui_silica";

    s_gui_silica.gui.plugin.name = "Silica GUI plugin";
    s_gui_silica.gui.plugin.descr = "User interface implemented using Sailfish Silica QML module";

    s_gui_silica.gui.plugin.copyright = "DeadBeef-Silica GUI plugin\n"
                                        "Copyright © 2017 Evgeny Kravchenko <cravchik@yandex.ru>\n"
                                        "\n"
                                        "This program is free software: you can redistribute it and/or modify "
                                        "it under the terms of the GNU General Public License as published by "
                                        "the Free Software Foundation, either version 3 of the License, or "
                                        "(at your option) any later version.\n"
                                        "\n"
                                        "This program is distributed in the hope that it will be useful, "
                                        "but WITHOUT ANY WARRANTY; without even the implied warranty of "
                                        "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the "
                                        "GNU General Public License for more details.\n"
                                        "\n"
                                        "You should have received a copy of the GNU General Public License "
                                        "along with this program.  If not, see <http://www.gnu.org/licenses/>.";

    s_gui_silica.gui.plugin.website = "https://bitbucket.org/kravich/deadbeef-silica";

    s_gui_silica.gui.plugin.start = ddb_gui_silica_start;
    s_gui_silica.gui.plugin.stop = ddb_gui_silica_stop;
    s_gui_silica.gui.plugin.connect = ddb_gui_silica_connect;
    s_gui_silica.gui.plugin.message = ddb_gui_silica_message;

    return DB_PLUGIN(&s_gui_silica);
}
