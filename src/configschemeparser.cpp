#include "configschemeparser.h"

#include "logger.h"

#include <QStringList>

CConfigSchemeParser::CConfigSchemeParser():
    m_consumer(NULL),
    m_parserState(ParserStateInitial),
    m_type(UnknownType),
    m_selectItemsCount(0)
{
}

void CConfigSchemeParser::Parse(QString scheme, IConfigSchemeConsumer *consumer)
{
    m_consumer = consumer;

    Tokenize(scheme);
}

void CConfigSchemeParser::Tokenize(QString scheme)
{
    enum TokenizerState
    {
        StateChar,
        StateWhitespace,
        StateQuote,
        StateSemicolon,
        StateError
    };

    TokenizerState state = StateSemicolon;
    bool insideQuotes = false;

    QString accumulator;

    OnSchemeBegin();

    for (int i = 0; i < scheme.count(); i++)
    {
        QChar c = scheme[i];

        if (state == StateChar)
        {
            if (c == ' ' ||
                c == '\r' ||
                c == '\n')
            {
                state = StateWhitespace;

                if (insideQuotes)
                {
                    accumulator.append(c);
                }
                else
                {
                    bool success = OnToken(accumulator);
                    accumulator.clear();

                    if (!success)
                    {
                        state = StateError;
                        break;
                    }
                }
            }
            else if (c == '"')
            {
                if (insideQuotes)
                {
                    insideQuotes = false;
                    state = StateQuote;

                    bool success = OnToken(accumulator);
                    accumulator.clear();

                    if (!success)
                    {
                        state = StateError;
                        break;
                    }
                }
                else
                {
                    state = StateError;
                    break;
                }
            }
            else if (c == ';')
            {
                state = StateSemicolon;

                if (insideQuotes)
                {
                    accumulator.append(c);
                }
                else
                {
                    bool success = OnToken(accumulator);
                    accumulator.clear();

                    if (success)
                    {
                        bool success = OnSeparator();

                        if (!success)
                        {
                            state = StateError;
                            break;
                        }
                    }
                    else
                    {
                        state = StateError;
                        break;
                    }
                }
            }
            else
            {
                // Stay in char state
                accumulator.append(c);
            }
        }
        else if (state == StateWhitespace)
        {
            if (c == ' ' ||
                c == '\r' ||
                c == '\n')
            {
                // stay in whitespace state

                if (insideQuotes)
                {
                    accumulator.append(c);
                }
            }
            else if (c == '"')
            {
                state = StateQuote;

                if (!insideQuotes)
                {
                    insideQuotes = true;
                }
                else
                {
                    insideQuotes = false;

                    bool success = OnToken(accumulator);
                    accumulator.clear();

                    if (!success)
                    {
                        state = StateError;
                        break;
                    }
                }
            }
            else if (c == ';')
            {
                state = StateSemicolon;

                bool success = OnSeparator();

                if (!success)
                {
                    state = StateError;
                    break;
                }
            }
            else
            {
                state = StateChar;
                accumulator.append(c);
            }
        }
        else if (state == StateQuote)
        {
            if (c == ' ' ||
                c == '\r' ||
                c == '\n')
            {
                state = StateWhitespace;

                if (insideQuotes)
                {
                    accumulator.append(c);
                }
            }
            else if (c == '"')
            {
                // stay in quote state

                if (insideQuotes)
                {
                    insideQuotes = false;

                    bool success = OnToken(accumulator);
                    accumulator.clear();

                    if (!success)
                    {
                        state = StateError;
                        break;
                    }
                }
                else
                {
                    insideQuotes = true;
                }
            }
            else if (c == ';')
            {
                state = StateSemicolon;

                if (!insideQuotes)
                {
                    bool success = OnSeparator();

                    if (!success)
                    {
                        state = StateError;
                        break;
                    }
                }
                else
                {
                    accumulator.append(c);
                }
            }
            else
            {
                if (insideQuotes)
                {
                    accumulator.append(c);
                }
                else
                {
                    state = StateError;
                    break;
                }
            }
        }
        else if (state == StateSemicolon)
        {
            if (c == ' ' ||
                c == '\r' ||
                c == '\n')
            {
                state = StateWhitespace;

                if (insideQuotes)
                {
                    accumulator.append(c);
                }
            }
            else if (c == '"')
            {
                state = StateQuote;

                if (insideQuotes)
                {
                    insideQuotes = false;

                    bool success = OnToken(accumulator);
                    accumulator.clear();

                    if (!success)
                    {
                        state = StateError;
                        break;
                    }
                }
                else
                {
                    insideQuotes = true;
                }
            }
            else if (c == ';')
            {
                if (insideQuotes)
                {
                    // stay in semicolon state
                    accumulator.append(c);
                }
                else
                {
                    // FIXME: Handle as regular end of property?
                    state = StateError;
                    break;
                }
            }
            else
            {
                state = StateChar;
                accumulator.append(c);
            }
        }
    }

    if (state != StateError)
    {
        bool success = OnSchemeEnd();

        if (!success)
            OnError();
    }
    else
    {
        OnError();
    }
}

void CConfigSchemeParser::OnSchemeBegin()
{
    LOG_DBG("OnSchemeBegin");
    m_parserState = ParserStateInitial;
    m_type = UnknownType;
    m_selectItemsCount = 0;

    m_consumer->OnSchemeBegin();
}

bool CConfigSchemeParser::OnToken(QString token)
{
    LOG_DBG("Token %s", token.toUtf8().data());

    if (m_parserState == ParserStateInitial)
    {
        if (token == "property")
        {
            m_parserState = ParserStateKeyword;
            m_consumer->OnPropertyBegin();
        }
        else
        {
            m_parserState = ParserStateError;
        }
    }
    else if (m_parserState == ParserStateKeyword)
    {
        m_parserState = ParserStateLabel;
        m_consumer->OnLabel(token);
    }
    else if (m_parserState == ParserStateLabel)
    {
        m_parserState = ParserStatePropertyType;

        m_type = token == "checkbox"            ? CheckboxType :
                 token == "entry"               ? EntryType :
                 token == "password"            ? PasswordType :
                 token == "file"                ? FileType :
                 token.startsWith("hscale") ||
                 token.startsWith("spinbtn")    ? HscaleType :
                 token.startsWith("vscale")     ? VscaleType :
                 token.startsWith("select")     ? SelectType :
                                                  UnknownType;

        m_consumer->OnType(m_type);

        if (m_type == HscaleType ||
            m_type == VscaleType)
        {
            QString addParams = token.mid(6);

            if (token.startsWith("spinbtn"))
                addParams = addParams.mid(1);

            bool success = OnScaleAdditionalParams(addParams);

            if (!success)
                m_parserState = ParserStateError;
        }
        else if (m_type == SelectType)
        {
            QString addParams = token.mid(6);

            bool success = OnSelectAdditionalParams(addParams);

            if (!success)
                m_parserState = ParserStateError;
        }
    }
    else if (m_parserState == ParserStatePropertyType)
    {
        m_parserState = ParserStateKey;
        m_consumer->OnKey(token);
    }
    else if (m_parserState == ParserStateKey)
    {
        m_parserState = ParserStateDefaultValue;

        if (m_type == EntryType ||
            m_type == PasswordType ||
            m_type == FileType)
        {
            m_consumer->OnStringDefaultValue(token);
        }
        else if (m_type == CheckboxType)
        {
            m_consumer->OnBoolDefaultValue((bool)token.toInt());
        }
        else if (m_type == SelectType)
        {
            m_consumer->OnIntDefaultValue(token.toInt());
        }
        else if (m_type == HscaleType ||
                 m_type == VscaleType)
        {
            m_consumer->OnFloatDefaultValue(token.toFloat());
        }
        else
        {
            m_consumer->OnStringDefaultValue(token);
        }
    }
    else if (m_parserState == ParserStateDefaultValue)
    {
        // Select type allows additional arguments, other types are not
        if (m_type == SelectType)
        {
            if (m_selectItemsCount > 0)
            {
                m_parserState = ParserStateAdditionalArgument;
                m_consumer->OnSelectItem(token);
                m_selectItemsCount--;
            }
            else
            {
                m_parserState = ParserStateError;
            }
        }
        else
        {
            m_parserState = ParserStateError;
        }
    }
    else if (m_parserState == ParserStateAdditionalArgument)
    {
        if (m_selectItemsCount > 0)
        {
            // Stay in additional argument state
            m_consumer->OnSelectItem(token);
            m_selectItemsCount--;
        }
        else
        {
            m_parserState = ParserStateError;
        }
    }

    if (m_parserState == ParserStateError)
        return false;

    return true;
}

bool CConfigSchemeParser::OnScaleAdditionalParams(QString addParams)
{
    LOG_DBG("OnScaleAdditionalParams: addParams=%s", addParams.toUtf8().data());

    if (addParams.count() < QString("[0,0,0]").count())
        return false;

    if (!addParams.startsWith("[") ||
        !addParams.endsWith("]"))
        return false;

    // Filter out brakets
    addParams = addParams.mid(1, addParams.count() - 2);

    QStringList values = addParams.split(',');

    if (values.count() != 3)
        return false;

    for (int j = 0; j < 3; j++)
    {
        bool dotFound = false;

        for (int i = 0; i < values[j].count(); i++)
        {
            if (values[j][i] == '.')
            {
                if (!dotFound)
                    dotFound = true;
                else
                {
                    LOG_DBG("Double dot");
                    return false;
                }
            }
            else if (!values[j][i].isNumber())
            {
                LOG_DBG("Not number: %c", values[j][i].toLatin1());
                return false;
            }
        }
    }

    m_consumer->OnScaleMin(values[0].toFloat());
    m_consumer->OnScaleMax(values[1].toFloat());
    m_consumer->OnScaleStep(values[2].toFloat());

    return true;
}

bool CConfigSchemeParser::OnSelectAdditionalParams(QString addParams)
{
    LOG_DBG_FUNC("OnSelectAdditionalParams: addParams=%s", addParams.toUtf8().data());

    if (addParams.count() < QString("[0]").count())
        return false;

    if (!addParams.startsWith("[") ||
        !addParams.endsWith("]"))
        return false;

    // Filter out brakets
    addParams = addParams.mid(1, addParams.count() - 2);

    for (int i = 0; i < addParams.count(); i++)
        if (!addParams[i].isNumber())
            return false;

    m_selectItemsCount = addParams.toInt();

    m_consumer->OnSelectCount(m_selectItemsCount);

    return true;
}

bool CConfigSchemeParser::OnSeparator()
{
    LOG_DBG("OnSeparator");

    if (m_parserState == ParserStateDefaultValue ||
        (m_parserState == ParserStateAdditionalArgument && m_selectItemsCount == 0))
    {
        m_consumer->OnPropertyEnd();
        m_parserState = ParserStateInitial;
    }
    else
    {
        m_parserState = ParserStateError;
    }

    if (m_parserState == ParserStateError)
        return false;

    return true;
}

bool CConfigSchemeParser::OnSchemeEnd()
{
    LOG_DBG("OnSchemeEnd");

    if (m_parserState != ParserStateInitial)
        return false;

    m_consumer->OnSchemeEnd();

    return true;
}

void CConfigSchemeParser::OnError()
{
    LOG_DBG("OnErrorEnd");
    m_consumer->OnError();
}
