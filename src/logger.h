#ifndef __LOGGER_H
#define __LOGGER_H

#include <unistd.h>
#include <sys/syscall.h>
#include <sys/types.h>

#include <assert.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>

enum ELoggerSeverity
{
    LoggerSeverityError,
    LoggerSeverityWarning,
    LoggerSeverityDebug
};

void LoggerInit(ELoggerSeverity outputSeverity);

void LoggerOutput(ELoggerSeverity severity, const char *fmt, ...);

void LoggerOutputV(ELoggerSeverity severity, const char *fmt, va_list args);

// FIXME: Simplify this
class CLoggerFuncScoped
{
public:
    CLoggerFuncScoped(ELoggerSeverity severity,
                      const char *filename,
                      int line,
                      const char *func,
                      const char *fmt = NULL,
                      ...):
        m_severity(severity),
        m_filename(filename),
        m_line(line),
        m_func(func)
    {
        assert(filename);
        assert(line);
        assert(func);

        if (fmt)
        {
            char format[256];
            snprintf(format, 256, "[ddb_gui_silica][%ld][D] %s:%d Entering %s: %s\n", syscall(SYS_gettid), filename, line, func, fmt);

            va_list args;

            va_start(args, fmt);

            LoggerOutputV(severity, format, args);

            va_end(args);
        }
        else
        {
            LoggerOutput(severity, "[ddb_gui_silica][%ld][D] %s:%d Entering %s\n", syscall(SYS_gettid), filename, line, func);
        }
    }

    ~CLoggerFuncScoped()
    {
        LoggerOutput(m_severity, "[ddb_gui_silica][%ld][D] %s:%d Leaving %s\n", syscall(SYS_gettid), m_filename, m_line, m_func);
    }

private:
    ELoggerSeverity m_severity;
    const char *m_filename;
    int m_line;
    const char *m_func;
};

#define __FILENAME__ (strrchr("/" __FILE__, '/') + 1)

// FIXME: Move markers from macro to LoggerOutput() implementation?
// FIXME: gettid() is not cross-platform
#define LOG_ERR(fmt, ...)   LoggerOutput(LoggerSeverityError, "\033[31m[ddb_gui_silica][%ld][E] %s:%d " fmt "\033[0m\n", syscall(SYS_gettid), __FILENAME__, __LINE__, ##__VA_ARGS__)
#define LOG_WARN(fmt, ...)  LoggerOutput(LoggerSeverityWarning, "\033[33m[ddb_gui_silica][%ld][W] %s:%d " fmt "\033[0m\n", syscall(SYS_gettid), __FILENAME__, __LINE__, ##__VA_ARGS__)
#define LOG_DBG(fmt, ...)   LoggerOutput(LoggerSeverityDebug, "[ddb_gui_silica][%ld][D] %s:%d " fmt "\n", syscall(SYS_gettid), __FILENAME__, __LINE__, ##__VA_ARGS__)

#define LOG_DBG_FUNC(...)      CLoggerFuncScoped __scopedObj(LoggerSeverityDebug, __FILENAME__, __LINE__, __func__, ##__VA_ARGS__)

#endif // __LOGGER_H
