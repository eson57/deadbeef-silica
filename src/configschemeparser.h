#ifndef __CONFIGSCHEMEPARSER_H
#define __CONFIGSCHEMEPARSER_H

#include <QString>

// FIXME: Rename?
enum EConfigPropertyType
{
    EntryType,
    PasswordType,
    FileType,
    CheckboxType,
    HscaleType,
    VscaleType,
    SelectType,
    UnknownType
};

class IConfigSchemeConsumer
{
public:
    virtual void OnSchemeBegin() = 0;

    virtual void OnPropertyBegin() = 0;

    virtual void OnLabel(QString label) = 0;
    virtual void OnType(EConfigPropertyType type) = 0;
    virtual void OnScaleMin(float min) = 0;
    virtual void OnScaleMax(float max) = 0;
    virtual void OnScaleStep(float step) = 0;
    virtual void OnSelectCount(int count) = 0;
    virtual void OnKey(QString key) = 0;
    virtual void OnStringDefaultValue(QString defaultValue) = 0;
    virtual void OnIntDefaultValue(int defaultValue) = 0;
    virtual void OnBoolDefaultValue(bool defaultValue) = 0;
    virtual void OnFloatDefaultValue(float defaultValue) = 0;

    virtual void OnSelectItem(QString item) = 0;

    virtual void OnPropertyEnd() = 0;

    virtual void OnSchemeEnd() = 0;

    virtual void OnError() = 0;
};

class CConfigSchemeParser
{
public:
    CConfigSchemeParser();

    void Parse(QString scheme, IConfigSchemeConsumer *consumer);

private:
    enum EParserState
    {
        ParserStateInitial,
        ParserStateKeyword,
        ParserStateLabel,
        ParserStatePropertyType,
        ParserStateKey,
        ParserStateDefaultValue,
        ParserStateAdditionalArgument,
        ParserStateError
    };

    void Tokenize(QString scheme);

    void OnSchemeBegin();
    bool OnToken(QString token);
    bool OnScaleAdditionalParams(QString addParams);
    bool OnSelectAdditionalParams(QString addParams);
    bool OnSeparator();
    bool OnSchemeEnd();
    void OnError();

    IConfigSchemeConsumer *m_consumer;
    EParserState m_parserState;
    EConfigPropertyType m_type;
    int m_selectItemsCount;
};

#endif // __CONFIGSCHEMEPARSER_H
