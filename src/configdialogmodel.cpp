#include "configdialogmodel.h"

#include "strutils.h"
#include "logger.h"

CConfigDialogModel::CConfigDialogModel(DB_functions_t *api):
    m_api(api),
    m_ptr("0x0")
{
    LOG_DBG_FUNC("%p", this);
}

CConfigDialogModel::~CConfigDialogModel()
{
    LOG_DBG_FUNC("%p", this);
}

int CConfigDialogModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return m_properties.count();
}

QVariant CConfigDialogModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() ||
        index.row() < 0 ||
        index.row() > m_properties.count() - 1)
        return QVariant();

    if (role == LabelRole)
        return m_properties[index.row()].label;
    else if (role == TypeRole)
        return m_properties[index.row()].type;
    else if (role == KeyRole)
        return m_properties[index.row()].key;
    else if (role == InitialValueRole)
        return m_properties[index.row()].initialValue;
    else if (role == ValueRole)
        return m_properties[index.row()].value;
    else if (role == ScaleMinRole)
        return m_properties[index.row()].scaleParams.min;
    else if (role == ScaleMaxRole)
        return m_properties[index.row()].scaleParams.max;
    else if (role == ScaleStepRole)
        return m_properties[index.row()].scaleParams.step;
    else if (role == SelectCountRole)
        return m_properties[index.row()].selectCount;
    else if (role == SelectItemsRole)
        return m_properties[index.row()].selectItems;

    return QVariant();
}

bool CConfigDialogModel::setValue(int row, const QVariant &value)
{
    if (row < 0 ||
        row > m_properties.count() - 1)
        return false;

    LOG_DBG("Adjusting %s key value to %s", m_properties[row].key.toUtf8().data(), value.toString().toUtf8().data());

    m_properties[row].value = value;

    return true;
}

QString CConfigDialogModel::ptr()
{
    return m_ptr;
}

void CConfigDialogModel::setPtr(QString ptr)
{
    LOG_DBG_FUNC("ptr=%s", ptr.toUtf8().data());

    m_ptr = ptr;
    emit ptrChanged();

    DB_plugin_t *plug = (DB_plugin_t*)QStringToPtr(ptr);

    beginResetModel();

    QString dialogScheme = plug && plug->configdialog ? QString::fromUtf8(plug->configdialog) : "";

    CConfigSchemeParser parser;
    parser.Parse(dialogScheme, this);

    endResetModel();

    m_pluginName = plug ? (plug->name ? QString::fromUtf8(plug->name) : "Unnamed plugin") : "";
    emit pluginNameChanged();
}

QString CConfigDialogModel::pluginName()
{
    return m_pluginName;
}

void CConfigDialogModel::commitChanges()
{
    QVector<int> changedRoles;
    changedRoles.append(InitialValueRole);

    for (int i = 0; i < m_properties.count(); i++)
        if (m_properties[i].value != m_properties[i].initialValue)
        {
            SetProperty(m_properties[i].key, m_properties[i].value);
            m_properties[i].initialValue = m_properties[i].value;
            emit dataChanged(index(i), index(i), changedRoles);
        }

    m_api->conf_save();
    m_api->sendmessage(DB_EV_CONFIGCHANGED, 0, 0, 0);
}

void CConfigDialogModel::discardChanges()
{
    QVector<int> changedRoles;
    changedRoles.append(ValueRole);

    for (int i = 0; i < m_properties.count(); i++)
        if (m_properties[i].value != m_properties[i].initialValue)
        {
            m_properties[i].value = m_properties[i].initialValue;
            emit dataChanged(index(i), index(i), changedRoles);
        }
}

void CConfigDialogModel::OnSchemeBegin()
{
    m_properties.clear();
}

void CConfigDialogModel::OnPropertyBegin()
{
    m_accProperty.label = "";
    m_accProperty.type = UnknownType;
    m_accProperty.key = "";
    m_accProperty.initialValue.clear();
    m_accProperty.value.clear();
    m_accProperty.scaleParams.min = 0;
    m_accProperty.scaleParams.max = 0;
    m_accProperty.scaleParams.step = 0;
    m_accProperty.selectCount = 0;
    m_accProperty.selectItems.clear();
}

void CConfigDialogModel::OnLabel(QString label)
{
    m_accProperty.label = label;
}

void CConfigDialogModel::OnType(EConfigPropertyType type)
{
    // FIMXE: Is it correct to map values using cast?
    m_accProperty.type = (EPropertyType)type;
}

void CConfigDialogModel::OnKey(QString key)
{
    m_accProperty.key = key;
}

void CConfigDialogModel::OnScaleMin(float min)
{
    m_accProperty.scaleParams.min = min;
}

void CConfigDialogModel::OnScaleMax(float max)
{
    m_accProperty.scaleParams.max = max;
}

void CConfigDialogModel::OnScaleStep(float step)
{
    m_accProperty.scaleParams.step = step;
}

void CConfigDialogModel::OnSelectCount(int count)
{
    m_accProperty.selectCount = count;
}

void CConfigDialogModel::OnStringDefaultValue(QString defaultValue)
{
    m_accProperty.initialValue = GetStringProperty(m_accProperty.key, defaultValue);
}

void CConfigDialogModel::OnIntDefaultValue(int defaultValue)
{
    m_accProperty.initialValue = GetIntProperty(m_accProperty.key, defaultValue);
}

void CConfigDialogModel::OnBoolDefaultValue(bool defaultValue)
{
    m_accProperty.initialValue = GetBoolProperty(m_accProperty.key, defaultValue);
}

void CConfigDialogModel::OnFloatDefaultValue(float defaultValue)
{
    m_accProperty.initialValue = GetFloatProperty(m_accProperty.key, defaultValue);
}

void CConfigDialogModel::OnSelectItem(QString item)
{
    m_accProperty.selectItems.append(item);
}

void CConfigDialogModel::OnPropertyEnd()
{
    m_accProperty.value = m_accProperty.initialValue;
    m_properties.append(m_accProperty);
}

void CConfigDialogModel::OnSchemeEnd()
{
}

void CConfigDialogModel::OnError()
{
    LOG_ERR("Failed to parse scheme");
    m_properties.clear();
}

QVariant CConfigDialogModel::GetIntProperty(QString key, int defaultValue)
{
    return m_api->conf_get_int(key.toUtf8().data(), defaultValue);
}

QVariant CConfigDialogModel::GetBoolProperty(QString key, bool defaultValue)
{
    return m_api->conf_get_int(key.toUtf8().data(), (int)defaultValue) ? true :
                                                                         false;
}

QVariant CConfigDialogModel::GetStringProperty(QString key, QString defaultValue)
{
    m_api->conf_lock();

    QString value = QString::fromUtf8(m_api->conf_get_str_fast(key.toUtf8().data(),
                                                               defaultValue.toUtf8().data()));

    m_api->conf_unlock();

    return value;
}

QVariant CConfigDialogModel::GetFloatProperty(QString key, float defaultValue)
{
    return m_api->conf_get_float(key.toUtf8().data(), defaultValue);
}

void CConfigDialogModel::SetProperty(QString key, QVariant value)
{
    LOG_DBG("Setting %s key value to %s",
            key.toUtf8().data(),
            value.toString().toUtf8().data());

    if (value.type() == QVariant::String)
        m_api->conf_set_str(key.toUtf8().data(), value.toString().toUtf8().data());
    else if (value.type() == QVariant::Int)
        m_api->conf_set_int(key.toUtf8().data(), value.toInt());
    else if (value.type() == QVariant::Bool)
        m_api->conf_set_int(key.toUtf8().data(), value.toBool() ? 1 : 0);
    else if (value.type() == QVariant::LongLong)
        m_api->conf_set_int64(key.toUtf8().data(), value.toLongLong());
    else if (value.type() == QVariant::Double)
        m_api->conf_set_float(key.toUtf8().data(), value.toFloat());
    else
        LOG_WARN("Failed to set property with type %s", value.typeName());
}

QHash<int, QByteArray> CConfigDialogModel::roleNames() const
{
    QHash<int, QByteArray> roles;

    roles[LabelRole] = "label";
    roles[TypeRole] = "type";
    roles[KeyRole] = "key";
    roles[InitialValueRole] = "initialValue";
    roles[ValueRole] = "value";
    roles[ScaleMinRole] = "scaleMin";
    roles[ScaleMaxRole] = "scaleMax";
    roles[ScaleStepRole] = "scaleStep";
    roles[SelectCountRole] = "selectCount";
    roles[SelectItemsRole] = "selectItems";

    return roles;
}
