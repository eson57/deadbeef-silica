#include "strutils.h"

#include <stdio.h>

QString PtrToQString(void *ptr)
{
    char buff[256];
    snprintf(buff, 256, "%p", ptr);

    return QString::fromUtf8(buff);
}

void *QStringToPtr(QString str)
{
    void *ptr = NULL;

    sscanf(str.toUtf8().data(), "%p", &ptr);

    return ptr;
}
