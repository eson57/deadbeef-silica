#ifndef __CDDBAPI_H
#define __CDDBAPI_H

#include "trackmetadatamodel.h"
#include "configdialogmodel.h"
#include "abstractqmllistmodel.h"

#include <deadbeef/deadbeef.h>

#include <AudioResourceQt>

#include <QObject>
#include <QVector>
#include <QQmlListProperty>
#include <QTimer>
#include <QAbstractListModel>


class CPlayitemsModel: public QAbstractListModel
{
    Q_OBJECT

    Q_PROPERTY(int nowPlayingIdx READ nowPlayingIdx NOTIFY nowPlayingIdxChanged)

    // FIXME: This looks weakly related to playlist model. Move to CDdbApi?
    Q_PROPERTY(QString nowPlayingtTitle READ nowPlayingTitle NOTIFY nowPlayingTitleChanged)
    Q_PROPERTY(QString nowPlayingArtistAlbum READ nowPlayingArtistAlbum NOTIFY nowPlayingArtistAlbumChanged)

public:
    enum EPlayitemsRoles
    {
        FirstLineRole = Qt::UserRole,
        SecondLineRole,
        DurationRole,
        IsNowPlayingRole,
        IsInPlayqueueRole
    };

    enum ESortOrder
    {
        SortAscending,
        SortDescending,
        SortRandom
    };

    Q_ENUM(ESortOrder)

    CPlayitemsModel(DB_functions_t *api);
    ~CPlayitemsModel();

    int rowCount(const QModelIndex &parent = QModelIndex()) const;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

    int nowPlayingIdx();
    QString nowPlayingTitle();
    QString nowPlayingArtistAlbum();

    Q_INVOKABLE void sortByTf(QString fmt, ESortOrder sortOrder);

    void onPlaylistChanged();
    void onPlayqueueChanged();
    void onTrackChanged(int newTrackIdx);

signals:
    void nowPlayingIdxChanged();
    void nowPlayingTitleChanged();
    void nowPlayingArtistAlbumChanged();

protected:
    QHash<int, QByteArray> roleNames() const;

private:
    QString produceTFormattedStringForIndex(int itemIdx, const QString &format) const;
    QString produceTFormattedStringForPlayitem(DB_playItem_t *it, const QString &format) const;

    bool isInPlayqueue(int itemIdx) const;

    DB_functions_t *m_api;
    int m_currentItemsCount;
    int m_currentTrackIdx;
};

class CPlaylistsModel: public CAbstractQmlListModel
{
    Q_OBJECT

    Q_PROPERTY(int currentPlaylistIdx READ currentPlaylistIdx NOTIFY currentPlaylistIdxChanged)
    Q_PROPERTY(QString currentPlaylistTitle READ currentPlaylistTitle NOTIFY currentPlaylistTitleChanged)

public:
    enum EPlaylistsRoles
    {
        TitleRole = Qt::UserRole,
        IsActiveRole
    };

    CPlaylistsModel(DB_functions_t *api);
    ~CPlaylistsModel();

    int rowCount(const QModelIndex &parent = QModelIndex()) const;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

    bool setData(const QModelIndex &index, const QVariant &value, int role);

    int currentPlaylistIdx();
    QString currentPlaylistTitle();

    void onPlaylistSwitched();

signals:
    void currentPlaylistIdxChanged();
    void currentPlaylistTitleChanged();

protected:
    QHash<int, QByteArray> roleNames() const;

private:
    QString getPlaylistTitle(int playlistIdx) const;

    DB_functions_t *m_api;
    int m_currentPlaylistsCount;
    int m_currentPlaylistIdx;
};

class CPluginsModel: public CAbstractQmlListModel
{
    Q_OBJECT

public:
    enum EPluginsRoles
    {
        // FIXME: Is it good idea to serialize pointer to plugin as string?
        PtrRole = Qt::UserRole,
        PlugIdRole,
        NameRole,
        DescriptionRole,
        CopyrightRole,
        WebsiteRole,
        VersionMajorRole,
        VersionMinorRole,
        HasSettingsRole
    };

    CPluginsModel(DB_functions_t *api);

    void refreshPlugins();

    int rowCount(const QModelIndex &parent = QModelIndex()) const;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

protected:
    QHash<int, QByteArray> roleNames() const;

private:
    struct SPluginRecord
    {
        SPluginRecord(): versionMajor(0), versionMinor(0) {}
        QString ptr;
        QString plugId;
        QString name;
        QString description;
        QString copyright;
        QString website;
        int versionMajor;
        int versionMinor;
        bool hasSettings;
    };

    DB_functions_t *m_api;
    QVector<SPluginRecord> m_plugins;
};

class CEqualizerModel: public QObject
{
    Q_OBJECT

    Q_PROPERTY(bool available READ available NOTIFY availableChanged)
    Q_PROPERTY(bool enabled READ enabled WRITE setEnabled NOTIFY enabledChanged)

    Q_PROPERTY(float preamp READ preamp WRITE setPreamp NOTIFY preampChanged)
    Q_PROPERTY(float band55Hz READ band55Hz WRITE setBand55Hz NOTIFY band55HzChanged)
    Q_PROPERTY(float band77Hz READ band77Hz WRITE setBand77Hz NOTIFY band77HzChanged)
    Q_PROPERTY(float band110Hz READ band110Hz WRITE setBand110Hz NOTIFY band110HzChanged)
    Q_PROPERTY(float band156Hz READ band156Hz WRITE setBand156Hz NOTIFY band156HzChanged)
    Q_PROPERTY(float band220Hz READ band220Hz WRITE setBand220Hz NOTIFY band220HzChanged)
    Q_PROPERTY(float band311Hz READ band311Hz WRITE setBand311Hz NOTIFY band311HzChanged)
    Q_PROPERTY(float band440Hz READ band440Hz WRITE setBand440Hz NOTIFY band440HzChanged)
    Q_PROPERTY(float band622Hz READ band622Hz WRITE setBand622Hz NOTIFY band622HzChanged)
    Q_PROPERTY(float band880Hz READ band880Hz WRITE setBand880Hz NOTIFY band880HzChanged)
    Q_PROPERTY(float band1200Hz READ band1200Hz WRITE setBand1200Hz NOTIFY band1200HzChanged)
    Q_PROPERTY(float band1800Hz READ band1800Hz WRITE setBand1800Hz NOTIFY band1800HzChanged)
    Q_PROPERTY(float band2500Hz READ band2500Hz WRITE setBand2500Hz NOTIFY band2500HzChanged)
    Q_PROPERTY(float band3500Hz READ band3500Hz WRITE setBand3500Hz NOTIFY band3500HzChanged)
    Q_PROPERTY(float band5000Hz READ band5000Hz WRITE setBand5000Hz NOTIFY band5000HzChanged)
    Q_PROPERTY(float band7000Hz READ band7000Hz WRITE setBand7000Hz NOTIFY band7000HzChanged)
    Q_PROPERTY(float band10000Hz READ band10000Hz WRITE setBand10000Hz NOTIFY band10000HzChanged)
    Q_PROPERTY(float band14000Hz READ band14000Hz WRITE setBand14000Hz NOTIFY band14000HzChanged)
    Q_PROPERTY(float band20000Hz READ band20000Hz WRITE setBand20000Hz NOTIFY band20000HzChanged)

    bool available();
    bool enabled();

    float preamp();
    float band55Hz();
    float band77Hz();
    float band110Hz();
    float band156Hz();
    float band220Hz();
    float band311Hz();
    float band440Hz();
    float band622Hz();
    float band880Hz();
    float band1200Hz();
    float band1800Hz();
    float band2500Hz();
    float band3500Hz();
    float band5000Hz();
    float band7000Hz();
    float band10000Hz();
    float band14000Hz();
    float band20000Hz();

    void setEnabled(bool enabled);

    void setPreamp(float value);
    void setBand55Hz(float value);
    void setBand77Hz(float value);
    void setBand110Hz(float value);
    void setBand156Hz(float value);
    void setBand220Hz(float value);
    void setBand311Hz(float value);
    void setBand440Hz(float value);
    void setBand622Hz(float value);
    void setBand880Hz(float value);
    void setBand1200Hz(float value);
    void setBand1800Hz(float value);
    void setBand2500Hz(float value);
    void setBand3500Hz(float value);
    void setBand5000Hz(float value);
    void setBand7000Hz(float value);
    void setBand10000Hz(float value);
    void setBand14000Hz(float value);
    void setBand20000Hz(float value);

signals:
    void availableChanged();
    void enabledChanged();

    void preampChanged();
    void band55HzChanged();
    void band77HzChanged();
    void band110HzChanged();
    void band156HzChanged();
    void band220HzChanged();
    void band311HzChanged();
    void band440HzChanged();
    void band622HzChanged();
    void band880HzChanged();
    void band1200HzChanged();
    void band1800HzChanged();
    void band2500HzChanged();
    void band3500HzChanged();
    void band5000HzChanged();
    void band7000HzChanged();
    void band10000HzChanged();
    void band14000HzChanged();
    void band20000HzChanged();

public:
    CEqualizerModel(DB_functions_t *api);
    ~CEqualizerModel();

    void onPluginsLoaded();
    void onDspChainChanged();

private:
    enum EEqualizerParams
    {
        PARAM_PREAMP,
        PARAM_BAND_55HZ,
        PARAM_BAND_77HZ,
        PARAM_BAND_110HZ,
        PARAM_BAND_156HZ,
        PARAM_BAND_220HZ,
        PARAM_BAND_311HZ,
        PARAM_BAND_440HZ,
        PARAM_BAND_622HZ,
        PARAM_BAND_880HZ,
        PARAM_BAND_1200HZ,
        PARAM_BAND_1800HZ,
        PARAM_BAND_2500HZ,
        PARAM_BAND_3500HZ,
        PARAM_BAND_5000HZ,
        PARAM_BAND_7000HZ,
        PARAM_BAND_10000HZ,
        PARAM_BAND_14000HZ,
        PARAM_BAND_20000HZ,
        PARAMS_NUM
    };

    ddb_dsp_context_t* getEq();
    float getParam(EEqualizerParams param);
    void setParam(EEqualizerParams param, float value);

    void notifyAllPropertiesChanged();

    DB_functions_t *m_api;
};

class CDdbApi : public QObject
{
    Q_OBJECT

public:
    enum EPlaybackState
    {
        PlaybackStopped,
        PlaybackPaused,
        PlaybackPlaying
    };

    Q_ENUM(EPlaybackState)

    enum EPlaybackMode
    {
        LoopAll,
        LoopNone,
        LoopSingle
    };

    Q_ENUM(EPlaybackMode)

    enum EPlaybackOrder
    {
        OrderLinear,
        OrderShuffleTracks,
        OrderRandom,
        OrderShuffleAlbums
    };

    Q_ENUM(EPlaybackOrder)

    Q_PROPERTY(CPlayitemsModel* playitemsModel READ playitemsModel NOTIFY playitemsModelChanged)
    Q_PROPERTY(CPlaylistsModel* playlistsModel READ playlistsModel NOTIFY playlistsModelChanged)
    Q_PROPERTY(CPluginsModel* pluginsModel READ pluginsModel NOTIFY pluginsModelChanged)
    Q_PROPERTY(CEqualizerModel* equalizerModel READ equalizerModel NOTIFY equalizerModelChanged)

    Q_PROPERTY(EPlaybackState playbackState READ playbackState NOTIFY playbackStateChanged)
    Q_PROPERTY(float playbackPositionMs READ playbackPositionMs NOTIFY playbackPositionMsChanged)
    Q_PROPERTY(float playbackDurationMs READ playbackDurationMs NOTIFY playbackDurationMsChanged)

    Q_PROPERTY(EPlaybackOrder playbackOrder READ playbackOrder WRITE setPlaybackOrder NOTIFY playbackOrderChanged)
    Q_PROPERTY(EPlaybackMode playbackMode READ playbackMode WRITE setPlaybackMode NOTIFY playbackModeChanged)

    Q_PROPERTY(QString version READ version NOTIFY versionChanged)

    CDdbApi(DB_functions_t *api);
    ~CDdbApi();

    int onConnect();
    Q_INVOKABLE int onDdbMessage(uint32_t id, uintptr_t ctx, uint32_t p1, uint32_t p2);

    Q_INVOKABLE void openPaths(QStringList paths);
    Q_INVOKABLE void addPaths(QStringList paths);
    Q_INVOKABLE void addLocation(QString location);
    Q_INVOKABLE void loadPlaylist(QString path);
    Q_INVOKABLE bool savePlaylist(QString path);

    CPlayitemsModel* playitemsModel();
    CPlaylistsModel* playlistsModel();
    CPluginsModel* pluginsModel();
    CEqualizerModel* equalizerModel();

    Q_INVOKABLE CTrackMetadataModel* createTrackTagsModel();
    Q_INVOKABLE CTrackMetadataModel* createTrackPropertiesModel();

    Q_INVOKABLE CConfigDialogModel* createConfigDialogModel();

    EPlaybackState playbackState();

    float playbackPositionMs();
    float playbackDurationMs();

    EPlaybackOrder playbackOrder();
    void setPlaybackOrder(EPlaybackOrder order);

    EPlaybackMode playbackMode();
    void setPlaybackMode(EPlaybackMode mode);

    Q_INVOKABLE void playItemIdx(int idx);

    Q_INVOKABLE void prev();
    Q_INVOKABLE void playPause();
    Q_INVOKABLE void next();

    Q_INVOKABLE void setPlaybackPosition(float seekPositionMs);

    Q_INVOKABLE void deleteItemIdx(int idx);
    Q_INVOKABLE void clearPlaylist();

    // FIXME: Move to CPlaylistsModel
    Q_INVOKABLE int createPlaylist(QString name);
    Q_INVOKABLE void selectPlaylist(int idx);
    Q_INVOKABLE void deletePlaylist(int idx);

    Q_INVOKABLE void addToPlayqueue(int idx);
    Q_INVOKABLE void removeFromPlayqueue(int idx);

    Q_INVOKABLE QString confGetStr(QString key, QString def = "");
    Q_INVOKABLE void confSetStr(QString key, QString val);

    Q_INVOKABLE int confGetInt(QString key, int def = 0);
    Q_INVOKABLE void confSetInt(QString key, int val);

    Q_INVOKABLE bool isFormatSupported(QString filename);
    Q_INVOKABLE QStringList getSupportedPlaylistExtensions();

    QString version();

    QString getInstallPrefix();

signals:
    // NOTE: Suppress QML warning by declaring these signals that actually will never be used
    void playitemsModelChanged();
    void playlistsModelChanged();
    void pluginsModelChanged();
    void equalizerModelChanged();
    void versionChanged();

    void playbackStateChanged();
    void playbackPositionMsChanged();
    float playbackDurationMsChanged();
    void playbackOrderChanged();
    void playbackModeChanged();

public slots:
    void terminate();

private slots:
    void onPlaybackStateChanged();
    void onPlayposUpdate();

private:
    Q_DISABLE_COPY(CDdbApi)

    // Memory management
    void acquire(QObject *object);
    void release(QObject *object);

    Q_INVOKABLE void onPlaylistSwitched();
    Q_INVOKABLE void onPlaylistContentChanged();
    Q_INVOKABLE void onPlayqueueChanged();
    Q_INVOKABLE void onSongChanged(ddb_playItem_t *from, ddb_playItem_t *to);
    Q_INVOKABLE void onSongStarted();
    Q_INVOKABLE void onSongFinished();
    Q_INVOKABLE void onPaused();
    Q_INVOKABLE void onConfigChanged();
    Q_INVOKABLE void onPluginsLoaded();
    Q_INVOKABLE void onDspChainChanged();

    void updatePlayingStatus();
    int getPlayingItemIdx();
    void updatePlaybackDurationMs();
    void updatePlaybackOrder();
    void updatePlaybackMode();

    DB_functions_t *m_api;

    AudioResourceQt::AudioResource m_audioResource;

    CPlayitemsModel *m_playitemsModel;
    CPlaylistsModel *m_playlistsModel;
    CPluginsModel *m_pluginsModel;
    CEqualizerModel *m_equalizerModel;

    EPlaybackState m_playbackState;

    QTimer m_playposUpdateTimer;
    float m_playbackPositionMs;

    float m_playbackDurationMs;

    EPlaybackOrder m_playbackOrder;
    EPlaybackMode m_playbackMode;
};


#endif // CDDBAPI_H
