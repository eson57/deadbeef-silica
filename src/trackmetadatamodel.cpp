#include "trackmetadatamodel.h"

#include "logger.h"

// FIXME: Whole implementation of this model is horribly inefficent

static const char* standardMetaFields[][2] =
{
    {"artist", "Artist"},
    {"title", "Track Title"},
    {"album", "Album"},
    {"year", "Date"},
    {"track", "Track Number"},
    {"numtracks", "Total Tracks"},
    {"genre", "Genre"},
    {"composer", "Composer"},
    {"disc", "Disc Number"},
    {"numdiscs", "Total Disks"},
    {"comment", "Comment"},
    {NULL, NULL}
};

static const char* standardPropFields[][2] =
{
    {":URI", "Location"},
    {":TRACKNUM", "Subtrack Index"},
    {":DURATION", "Duration"},
    {":TAGS", "Tag Type(s)"},
    {":HAS_EMBEDDED_CUESHEET", "Embedded Cuesheet"},
    {":DECODER", "Codec"},
    {NULL, NULL}
};

static bool isStandardMetaField(const char *key)
{
    for (int i = 0; standardMetaFields[i][0]; i++)
        if (strcmp(standardMetaFields[i][0], key) == 0)
            return true;

    return false;
}

static bool isStandardPropField(const char *key)
{
    for (int i = 0; standardPropFields[i][0]; i++)
        if (strcmp(standardPropFields[i][0], key) == 0)
            return true;

    return false;
}

CTrackMetadataModel::CTrackMetadataModel(DB_functions_t *api, bool provideProperties):
    QAbstractListModel(NULL),
    m_api(api),
    m_provideProperties(provideProperties),
    m_idx(-1)
{
    LOG_DBG("CTrackMetadataModel: %p, provideProperties=%d", this, provideProperties);
    updateKeys();
}

CTrackMetadataModel::~CTrackMetadataModel()
{
    LOG_DBG("~CTrackMetadataModel: %p", this);
}

int CTrackMetadataModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return m_keys.count();
}

QVariant CTrackMetadataModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() ||
        index.row() < 0 ||
        index.row() >= m_keys.count())
        return QVariant();

    if (role == KeyRole)
        return m_keys[index.row()].key;
    else if(role == KeyTitleRole)
        return m_keys[index.row()].keyTitle;
    else if (role == ValueRole)
        return m_keys[index.row()].value;

    return QVariant();
}

int CTrackMetadataModel::idx()
{
    return m_idx;
}

void CTrackMetadataModel::setIdx(int idx)
{
    LOG_DBG("Setting idx to %d", idx);

    if (m_idx == idx)
        return;

    m_idx = idx;
    emit idxChanged();

    updateKeys();
}

QHash<int, QByteArray> CTrackMetadataModel::roleNames() const
{
    QHash<int, QByteArray> roles;

    roles[KeyRole] = "key";
    roles[KeyTitleRole] = "keyTitle";
    roles[ValueRole] = "value";

    return roles;
}

void CTrackMetadataModel::updateKeys()
{
    LOG_DBG_FUNC();

    beginResetModel();

    LOG_DBG("Clearing keys");

    m_keys.clear();

    if (m_idx < 0)
    {
        LOG_DBG("No idx set, nothing to do");
        endResetModel();
        return;
    }

    m_api->pl_lock();

    ddb_playItem_t *it = m_api->pl_get_for_idx(m_idx);

    if (it)
    {
        const char **fieldsArr = (const char**)standardMetaFields;

        if (m_provideProperties)
            fieldsArr = (const char**)standardPropFields;

        for (int i = 0; fieldsArr[i * 2 + 0]; i++)
        {
            const char *val = m_api->pl_find_meta_raw(it, fieldsArr[i * 2 + 0]);

            LOG_DBG("Checked standard field %s, it's value is %s", fieldsArr[i * 2 + 0], val);

            if (val)
            {
                SMetaRecord metaRec;

                metaRec.key = QString::fromUtf8(fieldsArr[i * 2 + 0]);
                metaRec.keyTitle = QString::fromUtf8(fieldsArr[i * 2 + 1]);
                metaRec.value = QString::fromUtf8(val);

                m_keys.append(metaRec);
            }
        }

        DB_metaInfo_t *meta = m_api->pl_get_metadata_head(it);

        while (meta)
        {
            bool isNonStandardMeta = meta->key[0] != '!' &&
                                     meta->key[0] != ':' &&
                                     !isStandardMetaField(meta->key);

            bool isNonStandardProp = meta->key[0] != '!' &&
                                     meta->key[0] == ':' &&
                                     !isStandardPropField(meta->key);

            LOG_DBG("Checked field %s with value %s - isNonStandardMeta=%d, isNonStandardProp=%d",
                    meta->key,
                    meta->value,
                    isNonStandardMeta,
                    isNonStandardProp);

            if ((!m_provideProperties && isNonStandardMeta) ||
                (m_provideProperties && isNonStandardProp))
            {
                SMetaRecord metaRec;

                metaRec.key = QString::fromUtf8(meta->key);
                metaRec.keyTitle = QString::fromUtf8(meta->key);
                metaRec.value = QString::fromUtf8(meta->value);

                m_keys.append(metaRec);
            }

            meta = meta->next;
        }

        m_api->pl_item_unref(it);
    }

    m_api->pl_unlock();

    endResetModel();
}
