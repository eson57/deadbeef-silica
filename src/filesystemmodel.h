#ifndef __FILESYSTEMMODEL_H
#define __FILESYSTEMMODEL_H

#include <QAbstractListModel>
#include <QDir>
#include <QJSValue>

class CFileSystemModel: public QAbstractListModel
{
    Q_OBJECT

    Q_PROPERTY(QString path READ path WRITE setPath NOTIFY pathChanged)
    Q_PROPERTY(bool showHidden READ showHidden WRITE setShowHidden NOTIFY showHiddenChanged)
    Q_PROPERTY(bool multiple READ multiple WRITE setMultiple NOTIFY multipleChanged)
    Q_PROPERTY(bool selectFiles READ selectFiles WRITE setSelectFiles NOTIFY selectFilesChanged)
    Q_PROPERTY(bool selectDirectories READ selectDirectories WRITE setSelectDirectories NOTIFY selectDirectoriesChanged)

    Q_PROPERTY(QStringList filterNames READ filterNames NOTIFY filterNamesChanged)
    Q_PROPERTY(int activeFilterIndex READ activeFilterIndex WRITE setActiveFilterIndex NOTIFY activeFilterIndexChanged)

    // FIXME: Looks like this is very inefficient
    Q_PROPERTY(QStringList selectedPaths READ selectedPaths NOTIFY selectedPathsChanged)

public:
    enum EFileSystemModelRoles
    {
        FileNameRole = Qt::UserRole,
        FilePathRole,
        IsDirectoryRole,
        CheckableRole,
        CheckedRole
    };

    CFileSystemModel(QObject *parent = 0);
    ~CFileSystemModel();

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

    QString path();
    void setPath(QString path);

    bool showHidden();
    void setShowHidden(bool show);

    bool multiple();
    void setMultiple(bool multiple);

    bool selectFiles();
    void setSelectFiles(bool select);

    bool selectDirectories();
    void setSelectDirectories(bool select);

    Q_INVOKABLE int installFilter(QString name, QJSValue functor);
    Q_INVOKABLE void removeFilter(int index);

    QStringList filterNames();

    int activeFilterIndex();
    void setActiveFilterIndex(int index);

    QStringList selectedPaths();

    // FIXME: Use native Qt's selection mechanism?
    Q_INVOKABLE bool setCheckedState(int row, bool checked);

signals:
    void pathChanged();
    void showHiddenChanged();
    void multipleChanged();
    void selectFilesChanged();
    void selectDirectoriesChanged();
    void filterNamesChanged();
    void activeFilterIndexChanged();
    void selectedPathsChanged();

protected:
    QHash<int, QByteArray> roleNames() const;

private:
    bool testNameAgainstFilter(QString fileName);
    void refreshFilesList();

    QDir m_dir;
    QFileInfoList m_filesList;
    QVector<bool> m_checkStates;

    bool m_multiple;

    bool m_selectFiles;
    bool m_selectDirectories;

    void acquire(QObject *object);
    void release(QObject *object);

    void clearAndReleaseFilters();
    void updateFilter();

    struct SFilterRecord
    {
        QString name;
        QJSValue functor;
    };

    QVector<SFilterRecord> m_filters;
    int m_activeFilterIndex;
};

#endif // __FILESYSTEMMODEL_H
