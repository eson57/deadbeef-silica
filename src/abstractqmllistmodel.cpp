#include "abstractqmllistmodel.h"

#include "logger.h"

CAbstractQmlListModel::CAbstractQmlListModel(QObject *parent):
    QAbstractListModel(parent)
{
    connect(this, &QAbstractListModel::rowsInserted, this, &CAbstractQmlListModel::countChanged);
    connect(this, &QAbstractListModel::rowsMoved, this, &CAbstractQmlListModel::countChanged);
    connect(this, &QAbstractListModel::rowsRemoved, this, &CAbstractQmlListModel::countChanged);
}

QVariantMap CAbstractQmlListModel::get(int row)
{
    QHash<int, QByteArray> roles = roleNames();
    QModelIndex recordIndex = index(row, 0);

    QVariantMap object;

    for (QHash<int, QByteArray>::iterator roleIt = roles.begin();
         roleIt != roles.end();
         roleIt++)
    {
        object[roleIt.value()] = recordIndex.data(roleIt.key());
    }

    return object;
}

bool CAbstractQmlListModel::setProperty(int row, QString roleName, QVariant value)
{
    QModelIndex recordIndex = index(row);

    if (!recordIndex.isValid())
    {
        LOG_ERR("Can't set property for role '%s' for row %d: no such row", roleName.toUtf8().constData(), row);
        return false;
    }

    QHash<int, QByteArray> roles = roleNames();

    int role = roles.key(roleName.toUtf8().constData(), -1);

    if (role == -1)
    {
        LOG_ERR("Can't set property for role '%s' for row %d: no such role", roleName.toUtf8().constData(), row);
        return false;
    }

    return setData(recordIndex, value, role);
}
