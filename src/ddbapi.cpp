#include "ddbapi.h"

#include "strutils.h"
#include "logger.h"
#include "version.h"

#include <QQmlEngine>
#include <QQuickView>
#include <QFileInfo>

#include <algorithm>
#include <assert.h>

static const int playposUpdateIntervalMs = 500;

CPlayitemsModel::CPlayitemsModel(DB_functions_t *api):
    QAbstractListModel(NULL),
    m_api(api),
    m_currentItemsCount(0),
    m_currentTrackIdx(-1)
{
    LOG_DBG_FUNC("%p", this);
    onPlaylistChanged();
}

CPlayitemsModel::~CPlayitemsModel()
{
    LOG_DBG_FUNC("%p", this);
}

int CPlayitemsModel::nowPlayingIdx()
{
    return m_currentTrackIdx;
}

QString CPlayitemsModel::nowPlayingTitle()
{
    QString title;

    DB_playItem_t *it = m_api->streamer_get_playing_track();

    if (it)
    {
        title = produceTFormattedStringForPlayitem(it, "%title%");
        m_api->pl_item_unref(it);
    }

    return title;
}

QString CPlayitemsModel::nowPlayingArtistAlbum()
{
    QString artistAlbum;

    DB_playItem_t *it = m_api->streamer_get_playing_track();

    if (it)
    {
        artistAlbum = produceTFormattedStringForPlayitem(it, "$if(%artist%,%artist%,Unknown Artist)[ - %album%]");
        m_api->pl_item_unref(it);
    }

    return artistAlbum;
}

void CPlayitemsModel::sortByTf(QString fmt, ESortOrder sortOrder)
{
    m_api->pl_lock();

    ddb_playlist_t *plt = m_api->plt_get_curr();
    m_api->plt_sort_v2(plt, PL_MAIN, -1, fmt.toUtf8().data(), sortOrder == SortAscending  ? DDB_SORT_ASCENDING :
                                                              sortOrder == SortDescending ? DDB_SORT_DESCENDING :
                                                                                            DDB_SORT_RANDOM);
    m_api->plt_save_config(plt);
    m_api->plt_unref(plt);

    m_api->sendmessage(DB_EV_PLAYLISTCHANGED, 0, DDB_PLAYLIST_CHANGE_CONTENT, 0);

    m_api->pl_unlock();
}

int CPlayitemsModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return m_currentItemsCount;
}

QVariant CPlayitemsModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() ||
        index.row() < 0 ||
        index.row() >= m_currentItemsCount)
        return QVariant();

    LOG_DBG("Querying data of %d item for role %d", index.row(), role);

    if (role == FirstLineRole)
        return produceTFormattedStringForIndex(index.row(), "[(Q:%queue_indexes%) ][%tracknumber%. ]%title%");
    else if (role == SecondLineRole)
        return produceTFormattedStringForIndex(index.row(), "$if(%artist%,%artist%,Unknown Artist)[ - %album%]");
    else if (role == DurationRole)
        return produceTFormattedStringForIndex(index.row(), "%length%");
    else if (role == IsNowPlayingRole)
        return index.row() == m_currentTrackIdx;
    else if (role == IsInPlayqueueRole)
        return isInPlayqueue(index.row());

    return QVariant();
}

QString CPlayitemsModel::produceTFormattedStringForIndex(int itemIdx, const QString &format) const
{
    m_api->pl_lock();

    ddb_playItem_t *it = m_api->pl_get_for_idx(itemIdx);
    if (!it)
    {
        LOG_WARN("Can't get item for idx %d", itemIdx);
        m_api->pl_unlock();
        return "";
    }

    QString str = produceTFormattedStringForPlayitem(it, format);

    m_api->pl_item_unref(it);
    m_api->pl_unlock();

    return str;
}

QString CPlayitemsModel::produceTFormattedStringForPlayitem(DB_playItem_t *it, const QString &format) const
{
    ddb_tf_context_t tfCtx =
    {
        sizeof(ddb_tf_context_t),
        DDB_TF_CONTEXT_NO_DYNAMIC,
        it,
        NULL,
        0,
        0,
        PL_MAIN,
        0
    };

    char *tfBytecode = m_api->tf_compile(format.toUtf8().data());

    char output[256];
    int res = m_api->tf_eval(&tfCtx, tfBytecode, output, 256);

    m_api->tf_free(tfBytecode);

    if (res < 0)
    {
        LOG_ERR("Failed to produce titleformatted string, format: %s", format.toUtf8().data());
        return "";
    }

    return output;
}

bool CPlayitemsModel::isInPlayqueue(int itemIdx) const
{
    bool isInPlayqueue = false;

    DB_playItem_t *it = m_api->pl_get_for_idx(itemIdx);

    if (it)
    {
        isInPlayqueue = m_api->playqueue_test(it) >= 0;
        m_api->pl_item_unref(it);
    }
    else
    {
        LOG_ERR("Can't check if track with index %d is in playqueue: playitem query failed", itemIdx);
    }

    return isInPlayqueue;
}

QHash<int, QByteArray> CPlayitemsModel::roleNames() const
{
    QHash<int, QByteArray> roles;

    roles[FirstLineRole] = "firstLine";
    roles[SecondLineRole] = "secondLine";
    roles[DurationRole] = "duration";
    roles[IsNowPlayingRole] = "isNowPlaying";
    roles[IsInPlayqueueRole] = "isInPlayqueue";

    return roles;
}

void CPlayitemsModel::onPlaylistChanged()
{
    m_api->pl_lock();

    int newItemsCount = m_api->pl_getcount(PL_MAIN);

    int tracksChanged = std::min(m_currentItemsCount, newItemsCount);

    if (tracksChanged)
    {
        emit dataChanged(index(0), index(tracksChanged - 1));
    }

    if (newItemsCount > m_currentItemsCount)
    {
        beginInsertRows(QModelIndex(), tracksChanged, newItemsCount - 1);
        m_currentItemsCount = newItemsCount;
        endInsertRows();
    }
    else if (m_currentItemsCount > newItemsCount)
    {
        beginRemoveRows(QModelIndex(), tracksChanged, m_currentItemsCount - 1);
        m_currentItemsCount = newItemsCount;
        endRemoveRows();
    }

    // Playlist could be changed because network stream updated its track info,
    // so, emit track title/artist change signals
    emit nowPlayingTitleChanged();
    emit nowPlayingArtistAlbumChanged();

    m_api->pl_unlock();
}

void CPlayitemsModel::onPlayqueueChanged()
{
    if (m_currentItemsCount == 0)
        return;

    QVector<int> changedRoles;
    changedRoles.append(FirstLineRole); // Because first line contains playqueue index

    emit dataChanged(index(0), index(m_currentItemsCount - 1), changedRoles);
}

void CPlayitemsModel::onTrackChanged(int newTrackIdx)
{
    if (newTrackIdx == m_currentTrackIdx)
        return;

    int oldCurrentTrackIdx = m_currentTrackIdx;
    m_currentTrackIdx = newTrackIdx;

    emit nowPlayingIdxChanged();
    emit nowPlayingTitleChanged();
    emit nowPlayingArtistAlbumChanged();

    QVector<int> changedRoles;
    changedRoles.append(IsNowPlayingRole);

    emit dataChanged(index(oldCurrentTrackIdx), index(oldCurrentTrackIdx), changedRoles);
    emit dataChanged(index(m_currentTrackIdx), index(m_currentTrackIdx), changedRoles);
}

CPlaylistsModel::CPlaylistsModel(DB_functions_t *api):
    m_api(api),
    m_currentPlaylistsCount(0),
    m_currentPlaylistIdx(-1)
{
    LOG_DBG_FUNC("%p", this);
    onPlaylistSwitched();
}

CPlaylistsModel::~CPlaylistsModel()
{
    LOG_DBG_FUNC("%p", this);
}

int CPlaylistsModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return m_currentPlaylistsCount;
}

QVariant CPlaylistsModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() ||
        index.row() < 0 ||
        index.row() >= m_currentPlaylistsCount)
        return QVariant();

    if (role == TitleRole)
        return getPlaylistTitle(index.row());
    else if (role == IsActiveRole)
        return index.row() == m_currentPlaylistIdx;

    return QVariant();
}

bool CPlaylistsModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (!index.isValid())
    {
        LOG_ERR("Can't set property with role %d: provided index is invalid", role);
        return false;
    }

    if (role != TitleRole)
    {
        LOG_ERR("Can't change playlist property other than 'title' (attempted to change role %d)", role);
        return false;
    }

    if (value.type() != QVariant::String)
    {
        LOG_ERR("Can't update title of playlist with index %d: provided value is not a string", index.row());
        return false;
    }

    ddb_playlist_t *plt = m_api->plt_get_for_idx(index.row());

    if (!plt)
    {
        LOG_ERR("Can't update title of playlist with index %d: no such playlist found", index.row());
        return false;
    }

    LOG_DBG("Renaming playlist %d to '%s'", index.row(), value.toString().toUtf8().constData());

    m_api->plt_set_title(plt, value.toString().toUtf8().constData());
    m_api->plt_unref(plt);

    QVector<int> changedRoles;
    changedRoles.append(TitleRole);

    emit dataChanged(index, index, changedRoles);

    if (index.row() == m_currentPlaylistIdx)
        emit currentPlaylistTitleChanged();

    return true;
}

QString CPlaylistsModel::getPlaylistTitle(int playlistIdx) const
{
    ddb_playlist_t *plt = m_api->plt_get_for_idx(playlistIdx);

    if (!plt)
    {
        LOG_WARN("Playlist with idx %d was not found", playlistIdx);
        return "";
    }

    char titleBuff[256];
    m_api->plt_get_title(plt, titleBuff, 256);

    m_api->plt_unref(plt);

    return titleBuff;
}

int CPlaylistsModel::currentPlaylistIdx()
{
    return m_currentPlaylistIdx;
}

QString CPlaylistsModel::currentPlaylistTitle()
{
    return getPlaylistTitle(m_currentPlaylistIdx);
}

void CPlaylistsModel::onPlaylistSwitched()
{
    int newCurrentPlaylistIdx = m_api->plt_get_curr_idx();

    if (m_currentPlaylistIdx != newCurrentPlaylistIdx)
    {
        m_currentPlaylistIdx = newCurrentPlaylistIdx;
        emit currentPlaylistIdxChanged();
    }

    emit currentPlaylistTitleChanged();

    int newPlaylistsCount = m_api->plt_get_count();

    int playlistsChanged = std::min(m_currentPlaylistsCount, newPlaylistsCount);

    if (playlistsChanged)
    {
        emit dataChanged(index(0), index(playlistsChanged - 1));
    }

    if (newPlaylistsCount > m_currentPlaylistsCount)
    {
        beginInsertRows(QModelIndex(), playlistsChanged, newPlaylistsCount - 1);
        m_currentPlaylistsCount = newPlaylistsCount;
        endInsertRows();
    }
    else if (m_currentPlaylistsCount > newPlaylistsCount)
    {
        beginRemoveRows(QModelIndex(), playlistsChanged, m_currentPlaylistsCount - 1);
        m_currentPlaylistsCount = newPlaylistsCount;
        endRemoveRows();
    }
}

QHash<int, QByteArray> CPlaylistsModel::roleNames() const
{
    QHash<int, QByteArray> roles;

    roles[TitleRole] = "title";
    roles[IsActiveRole] = "isActive";

    return roles;
}

CDdbApi::CDdbApi(DB_functions_t *api):
    QObject(NULL),
    m_api(api),
    m_audioResource(this, AudioResourceQt::AudioResource::MediaType),
    m_playitemsModel(new CPlayitemsModel(m_api)),
    m_playlistsModel(new CPlaylistsModel(m_api)),
    m_pluginsModel(new CPluginsModel(m_api)),
    m_equalizerModel(new CEqualizerModel(m_api)),
    m_playbackState(PlaybackStopped),
    m_playbackPositionMs(0.0),
    m_playbackDurationMs(0.0),
    m_playbackOrder(OrderLinear),
    m_playbackMode(LoopAll)
{
    LOG_DBG_FUNC("%p", this);

    qRegisterMetaType<ddb_playItem_t*>("ddb_playItem_t*");

    // DO NOT FORGET TO UPDATE THIS LIST ONCE NEW TYPE IS IMPLEMENTED
    qmlRegisterUncreatableType<CPlayitemsModel>("deadbeef", 1, 0, "PlayitemsModel", "Use CDdbApi::playitemsModel factory property to access instance of this object");
    qmlRegisterUncreatableType<CPlaylistsModel>("deadbeef", 1, 0, "PlaylistsModel", "Use CDdbApi::playlistsModel factory property to access instance of this object");
    qmlRegisterUncreatableType<CTrackMetadataModel>("deadbeef", 1, 0, "TrackMetadataModel", "Use CDdbApi::createTrackTagsModel() and CDdbApi::createTrackPropertiesModel() factory methods to create instances of this object");
    qmlRegisterUncreatableType<CConfigDialogModel>("deadbeef", 1, 0, "ConfigDialogModel", "Use CDdbApi::createConfigDialogModel()factory methods to create instances of this object");

    acquire(m_playitemsModel);
    acquire(m_playlistsModel);
    acquire(m_pluginsModel);
    acquire(m_equalizerModel);

    connect(this, &CDdbApi::playbackStateChanged, this, &CDdbApi::onPlaybackStateChanged);

    m_playposUpdateTimer.setInterval(playposUpdateIntervalMs);
    connect(&m_playposUpdateTimer, &QTimer::timeout, this, &CDdbApi::onPlayposUpdate);

    updatePlaybackOrder();
    updatePlaybackMode();
}

CDdbApi::~CDdbApi()
{
    LOG_DBG_FUNC("%p", this);
    release(m_equalizerModel);
    release(m_pluginsModel);
    release(m_playlistsModel);
    release(m_playitemsModel);
}

int CDdbApi::onConnect()
{
    LOG_DBG_FUNC();
    m_pluginsModel->refreshPlugins();
    return 0;
}

static const char* GetDdbMessageName(uint32_t id)
{
    return id == DB_EV_NEXT ? "DB_EV_NEXT" :
           id == DB_EV_PREV ? "DB_EV_PREV" :
           id == DB_EV_PLAY_CURRENT ? "DB_EV_PLAY_CURRENT" :
           id == DB_EV_PLAY_NUM ? "DB_EV_PLAY_NUM" :
           id == DB_EV_STOP ? "DB_EV_STOP" :
           id == DB_EV_PAUSE ? "DB_EV_PAUSE" :
           id == DB_EV_PLAY_RANDOM ? "DB_EV_PLAY_RANDOM" :
           id == DB_EV_TERMINATE ? "DB_EV_TERMINATE" :
           id == DB_EV_PLAYLIST_REFRESH ? "DB_EV_PLAYLIST_REFRESH" :
           id == DB_EV_REINIT_SOUND ? "DB_EV_REINIT_SOUND" :
           id == DB_EV_CONFIGCHANGED ? "DB_EV_CONFIGCHANGED" :
           id == DB_EV_TOGGLE_PAUSE ? "DB_EV_TOGGLE_PAUSE" :
           id == DB_EV_ACTIVATED ? "DB_EV_ACTIVATED" :
           id == DB_EV_PAUSED ? "DB_EV_PAUSED" :
           id == DB_EV_PLAYLISTCHANGED ? "DB_EV_PLAYLISTCHANGED" :
           id == DB_EV_VOLUMECHANGED ? "DB_EV_VOLUMECHANGED" :
           id == DB_EV_OUTPUTCHANGED ? "DB_EV_OUTPUTCHANGED" :
           id == DB_EV_PLAYLISTSWITCHED ? "DB_EV_PLAYLISTSWITCHED" :
           id == DB_EV_SEEK ? "DB_EV_SEEK" :
           id == DB_EV_ACTIONSCHANGED ? "DB_EV_ACTIONSCHANGED" :
           id == DB_EV_DSPCHAINCHANGED ? "DB_EV_DSPCHAINCHANGED" :
           id == DB_EV_SELCHANGED ? "DB_EV_SELCHANGED" :
           id == DB_EV_PLUGINSLOADED ? "DB_EV_PLUGINSLOADED" :
           id == DB_EV_FOCUS_SELECTION ? "DB_EV_FOCUS_SELECTION" :
           id == DB_EV_SONGCHANGED ? "DB_EV_SONGCHANGED" :
           id == DB_EV_SONGSTARTED ? "DB_EV_SONGSTARTED" :
           id == DB_EV_SONGFINISHED ? "DB_EV_SONGFINISHED" :
           id == DB_EV_TRACKINFOCHANGED ? "DB_EV_TRACKINFOCHANGED" :
           id == DB_EV_SEEKED ? "DB_EV_SEEKED" :
           id == DB_EV_TRACKFOCUSCURRENT ? "DB_EV_TRACKFOCUSCURRENT" :
                                           "UNKNOWN_EVENT";
}

int CDdbApi::onDdbMessage(uint32_t id, uintptr_t ctx, uint32_t p1, uint32_t p2)
{
    LOG_DBG("message: id=%s, ctx=%p, p1=%u, p2=%u", GetDdbMessageName(id), ctx, p1, p2);

    switch (id)
    {
    case DB_EV_PLAYLISTSWITCHED:
        QMetaObject::invokeMethod(this, "onPlaylistSwitched");
        break;

    case DB_EV_PLAYLISTCHANGED:
        if (p1 == DDB_PLAYLIST_CHANGE_CONTENT)
        {
            QMetaObject::invokeMethod(this, "onPlaylistContentChanged");
        }
        else if (p1 == DDB_PLAYLIST_CHANGE_PLAYQUEUE)
        {
            QMetaObject::invokeMethod(this, "onPlayqueueChanged");
        }
        break;

    case DB_EV_SONGCHANGED:
        {
            ddb_event_trackchange_t *trackChangeEvent = (ddb_event_trackchange_t*)ctx;

            if (trackChangeEvent->from)
                m_api->pl_item_ref(trackChangeEvent->from);

            if (trackChangeEvent->to)
                m_api->pl_item_ref(trackChangeEvent->to);

            QMetaObject::invokeMethod(this,
                                      "onSongChanged",
                                      Q_ARG(ddb_playItem_t*, trackChangeEvent->from),
                                      Q_ARG(ddb_playItem_t*, trackChangeEvent->to));
        }
        break;

    case DB_EV_SONGSTARTED:
        QMetaObject::invokeMethod(this, "onSongStarted");
        break;

    case DB_EV_SONGFINISHED:
        QMetaObject::invokeMethod(this, "onSongFinished");
        break;

    case DB_EV_PAUSED:
        QMetaObject::invokeMethod(this, "onPaused");
        break;

    case DB_EV_CONFIGCHANGED:
        QMetaObject::invokeMethod(this, "onConfigChanged");
        break;

    case DB_EV_PLUGINSLOADED:
        QMetaObject::invokeMethod(this, "onPluginsLoaded");

    case DB_EV_DSPCHAINCHANGED:
        QMetaObject::invokeMethod(this, "onDspChainChanged");
    }

    return 0;
}

void CDdbApi::openPaths(QStringList paths)
{
    LOG_DBG_FUNC();

    // obtain playlist to work on
    ddb_playlist_t *plt = m_api->plt_get_curr();
    if (!plt)
    {
        LOG_ERR("Failed to get current playlist");
        return;
    }

    LOG_DBG("Clearing old playlist");

    m_api->plt_clear(plt);
    m_api->sendmessage(DB_EV_PLAYLISTCHANGED, 0, DDB_PLAYLIST_CHANGE_CONTENT, 0);

    LOG_DBG("Starting file add operation");

    if (m_api->plt_add_files_begin(plt, 0) < 0)
    {
        LOG_WARN("Can't start file add operation");
        m_api->plt_unref(plt);
        return;
    }

    for (int i = 0; i < paths.count(); i++)
    {
        QFileInfo fileInfo(paths[i]);

        int res = 0;

        if (fileInfo.isDir())
        {
            LOG_DBG("Adding dir %s", paths[i].toUtf8().data());
            res = m_api->plt_add_dir2(0, plt, paths[i].toUtf8().data(), NULL, NULL);
        }
        else
        {
            LOG_DBG("Adding file %s", paths[i].toUtf8().data());
            res = m_api->plt_add_file2(0, plt, paths[i].toUtf8().data(), NULL, NULL);
        }

        if (res < 0)
        {
            LOG_ERR("Failed to add %s to playlist", paths[i].toUtf8().data());
        }
    }

    // finish adding new files
    m_api->plt_add_files_end(plt, 0);

    m_api->plt_save_config(plt);

    // play from the beginning
    m_api->plt_set_cursor(plt, PL_MAIN, 0);

    m_api->conf_save();

    m_api->plt_unref(plt);

    m_api->sendmessage(DB_EV_PLAYLISTCHANGED, 0, DDB_PLAYLIST_CHANGE_CONTENT, 0);
    m_api->sendmessage(DB_EV_PLAY_NUM, 0, 0, 0);
}

void CDdbApi::addPaths(QStringList paths)
{
    LOG_DBG_FUNC();

    // obtain playlist to work on
    ddb_playlist_t *plt = m_api->plt_get_curr();
    if (!plt)
    {
        LOG_ERR("Failed to get current playlist");
        return;
    }

    LOG_DBG("Starting file add operation");

    if (m_api->plt_add_files_begin(plt, 0) < 0)
    {
        LOG_WARN("Can't start file add operation");
        m_api->plt_unref(plt);
        return;
    }

    for (int i = 0; i < paths.count(); i++)
    {
        QFileInfo fileInfo(paths[i]);

        int res = 0;

        if (fileInfo.isDir())
        {
            LOG_DBG("Adding dir %s", paths[i].toUtf8().data());
            res = m_api->plt_add_dir2(0, plt, paths[i].toUtf8().data(), NULL, NULL);
        }
        else
        {
            LOG_DBG("Adding file %s", paths[i].toUtf8().data());
            res = m_api->plt_add_file2(0, plt, paths[i].toUtf8().data(), NULL, NULL);
        }

        if (res < 0)
        {
            LOG_ERR("Failed to add %s to playlist", paths[i].toUtf8().data());
        }
    }

    // finish adding new files
    m_api->plt_add_files_end(plt, 0);

    m_api->plt_save_config(plt);

    m_api->conf_save();

    m_api->plt_unref(plt);

    //m_api->sendmessage(DB_EV_PLAYLISTCHANGED, 0, DDB_PLAYLIST_CHANGE_CONTENT, 0);
}

void CDdbApi::addLocation(QString location)
{
    LOG_DBG_FUNC();

    ddb_playlist_t *plt = m_api->plt_get_curr();

    LOG_DBG("Starting file add operation");

    if (m_api->plt_add_files_begin(plt, 0) < 0)
    {
        LOG_WARN("Can't start file add operation");
        m_api->plt_unref(plt);
        return;
    }

    LOG_DBG("Adding location %s", location.toUtf8().data());

    if (m_api->plt_add_file2(0, plt, location.toUtf8().data(), NULL, NULL) < 0)
    {
        LOG_ERR("Failed to add location %s to playlist", location.toUtf8().data());
    }

    m_api->plt_add_files_end(plt, 0);

    m_api->plt_save_config(plt);

    m_api->conf_save();

    m_api->plt_unref(plt);
}

void CDdbApi::loadPlaylist(QString path)
{
    LOG_DBG_FUNC();

    ddb_playlist_t * plt = m_api->plt_get_curr();
    if (!plt)
    {
        LOG_ERR("Failed to get current playlist");
        return;
    }

    LOG_DBG("Starting file add operation");

    // start adding new files
    if (m_api->plt_add_files_begin(plt, 0) < 0)
    {
        LOG_WARN("Can't start file add operation");
        m_api->plt_unref(plt);
        return;
    }

    LOG_DBG("Clearing playlist");

    m_api->plt_clear(plt);

    LOG_DBG("Loading playlist %s", path.toUtf8().data());

    m_api->plt_load2(0, plt, NULL, path.toUtf8().data(), NULL, NULL, NULL);
    m_api->plt_save_config(plt);

    m_api->plt_add_files_end(plt, 0);

    m_api->plt_unref(plt);

    m_api->sendmessage(DB_EV_PLAYLISTCHANGED, 0, DDB_PLAYLIST_CHANGE_CONTENT, 0);
}

bool CDdbApi::savePlaylist(QString path)
{
    LOG_DBG_FUNC();

    ddb_playlist_t *plt = m_api->plt_get_curr();

    if (!plt)
    {
        LOG_ERR("Failed to get current playlist");
        return false;
    }

    LOG_DBG("Saving playlist to %s", path.toUtf8().data());

    int res = m_api->plt_save(plt, NULL, NULL, path.toUtf8().data(), NULL, NULL, NULL);

    m_api->plt_unref(plt);

    if (res < 0)
    {
        LOG_ERR("Failed to save playlist to %s", path.toUtf8().data());
        return false;
    }

    return true;
}

CPlayitemsModel* CDdbApi::playitemsModel()
{
    return m_playitemsModel;
}

CPlaylistsModel* CDdbApi::playlistsModel()
{
    return m_playlistsModel;
}

CPluginsModel* CDdbApi::pluginsModel()
{
    return m_pluginsModel;
}

CEqualizerModel *CDdbApi::equalizerModel()
{
    return m_equalizerModel;
}

CTrackMetadataModel* CDdbApi::createTrackTagsModel()
{
    return new CTrackMetadataModel(m_api, false);
}

CTrackMetadataModel* CDdbApi::createTrackPropertiesModel()
{
    return new CTrackMetadataModel(m_api, true);
}

CConfigDialogModel* CDdbApi::createConfigDialogModel()
{
    return new CConfigDialogModel(m_api);
}

CDdbApi::EPlaybackState CDdbApi::playbackState()
{
    return m_playbackState;
}

float CDdbApi::playbackPositionMs()
{
    return m_playbackPositionMs;
}

float CDdbApi::playbackDurationMs()
{
    return m_playbackDurationMs;
}

CDdbApi::EPlaybackOrder CDdbApi::playbackOrder()
{
    return m_playbackOrder;
}

void CDdbApi::setPlaybackOrder(CDdbApi::EPlaybackOrder order)
{
    if (order == m_playbackOrder)
        return;

    LOG_DBG("Setting playback order to %d", order);

    m_playbackOrder = order;

    m_api->conf_set_int("playback.order", (int)m_playbackOrder);
    m_api->sendmessage(DB_EV_CONFIGCHANGED, 0, 0, 0);

    emit playbackOrderChanged();
}

CDdbApi::EPlaybackMode CDdbApi::playbackMode()
{
    return m_playbackMode;
}

void CDdbApi::setPlaybackMode(CDdbApi::EPlaybackMode mode)
{
    if (mode == m_playbackMode)
        return;

    LOG_DBG("Setting playback mode to %d", mode);

    m_playbackMode = mode;

    m_api->conf_set_int("playback.loop", (int)m_playbackMode);
    m_api->sendmessage(DB_EV_CONFIGCHANGED, 0, 0, 0);

    emit playbackModeChanged();
}

void CDdbApi::playItemIdx(int idx)
{
    LOG_DBG("Play item %d", idx);
    m_api->sendmessage(DB_EV_PLAY_NUM, 0, idx, 0);
}

void CDdbApi::prev()
{
    LOG_DBG("Prev item");
    m_api->sendmessage(DB_EV_PREV, 0, 0, 0);
}

void CDdbApi::playPause()
{
    if (playbackState() == PlaybackPlaying)
    {
        LOG_DBG("Pause");
        m_api->sendmessage(DB_EV_PAUSE, 0, 0, 0);
    }
    else
    {
        LOG_DBG("Play current");
        m_api->sendmessage(DB_EV_PLAY_CURRENT, 0, 0, 0);
    }
}

void CDdbApi::next()
{
    LOG_DBG("Next item");
    m_api->sendmessage(DB_EV_NEXT, 0, 0, 0);
}

void CDdbApi::setPlaybackPosition(float seekPositionMs)
{
    LOG_DBG("Seeking to %fms", seekPositionMs);

    m_api->sendmessage(DB_EV_SEEK, 0, (uint32_t)seekPositionMs, 0);

    // Fake playback position update to improve visual feedback
    m_playbackPositionMs = seekPositionMs;
    emit playbackPositionMsChanged();
}

void CDdbApi::deleteItemIdx(int idx)
{
    LOG_DBG("Deleting item %d", idx);

    m_api->pl_lock();

    ddb_playlist_t *plt = m_api->plt_get_curr();
    ddb_playItem_t *it = m_api->plt_get_item_for_idx(plt, idx, PL_MAIN);

    m_api->plt_remove_item(plt, it);

    m_api->pl_save_current();

    m_api->pl_item_unref(it);
    m_api->plt_unref(plt);

    m_api->pl_unlock();

    m_api->sendmessage(DB_EV_PLAYLISTCHANGED, 0, DDB_PLAYLIST_CHANGE_CONTENT, 0);
}

void CDdbApi::clearPlaylist()
{
    LOG_DBG("Clearing playlist");
    m_api->pl_clear();
    m_api->pl_save_current();
    m_api->sendmessage(DB_EV_PLAYLISTCHANGED, 0, DDB_PLAYLIST_CHANGE_CONTENT, 0);
}

int CDdbApi::createPlaylist(QString name)
{
    LOG_DBG("Creating playlist '%s'", name.toUtf8().data());

    m_api->pl_lock();

    int playlistsCount = m_api->plt_get_count();

    int newPlaylistIdx = m_api->plt_add(playlistsCount, name.toUtf8().data());

    m_api->pl_unlock();

    return newPlaylistIdx;
}

void CDdbApi::selectPlaylist(int idx)
{
    LOG_DBG("Selecting playlist %d", idx);
    m_api->plt_set_curr_idx(idx);
    // FIXME: Does it fix problem with multiple playlists loading on next start?
    m_api->conf_set_int("playlist.current", m_api->plt_get_curr_idx());
}

void CDdbApi::deletePlaylist(int idx)
{
    LOG_DBG("Deleting playlist %d", idx);
    m_api->plt_remove(idx);
    m_api->conf_save();
}

void CDdbApi::addToPlayqueue(int idx)
{
    DB_playItem_t *it = m_api->pl_get_for_idx(idx);

    if (!it)
    {
        LOG_ERR("Can't add track with idx %d into playqueue: playitem query failed", idx);
        return;
    }

    m_api->playqueue_push(it);
    m_api->pl_item_unref(it);
}

void CDdbApi::removeFromPlayqueue(int idx)
{
    DB_playItem_t *it = m_api->pl_get_for_idx(idx);

    if (!it)
    {
        LOG_ERR("Can't remove track with idx %d from playqueue: playitem query failed", idx);
        return;
    }

    m_api->playqueue_remove(it);
    m_api->pl_item_unref(it);
}

QString CDdbApi::confGetStr(QString key, QString def)
{
    m_api->conf_lock();
    QString value = QString::fromUtf8(m_api->conf_get_str_fast(key.toUtf8().data(),
                                                               def.toUtf8().data()));
    m_api->conf_unlock();

    return value;
}

void CDdbApi::confSetStr(QString key, QString val)
{
    LOG_DBG("Setting %s key to %s", key.toUtf8().data(), val.toUtf8().data());
    m_api->conf_set_str(key.toUtf8().data(), val.toUtf8().data());
    m_api->conf_save();
}

int CDdbApi::confGetInt(QString key, int def)
{
    return m_api->conf_get_int(key.toUtf8().constData(), def);
}

void CDdbApi::confSetInt(QString key, int val)
{
    LOG_DBG("Setting %s key to %d", key.toUtf8().constData(), val);
    m_api->conf_set_int(key.toUtf8().constData(), val);
    m_api->conf_save();
}

bool CDdbApi::isFormatSupported(QString filename)
{
    LOG_DBG_FUNC("filename=%s", filename.toUtf8().data());

    QFileInfo fileInfo(filename);

    DB_decoder_t **decoders = m_api->plug_get_decoder_list();

    for (int i = 0; decoders[i]; i++)
    {
        LOG_DBG("Checking decoder %d (%s)", i, decoders[i]->plugin.name ? decoders[i]->plugin.name :
                                                                          "<unknown>");

        if (decoders[i]->exts && decoders[i]->insert)
        {
            const char **exts = decoders[i]->exts;

            for (int e = 0; exts[e]; e++)
            {
                LOG_DBG("Checking ext %s", exts[e]);

                if (fileInfo.suffix().compare(exts[e], Qt::CaseInsensitive) == 0)
                {
                    LOG_DBG("Ext %s suits", exts[e]);
                    return true;
                }
            }
        }

        if (decoders[i]->prefixes && decoders[i]->insert)
        {
            const char **prefixes = decoders[i]->prefixes;

            for (int p = 0; prefixes[p]; p++)
            {
                LOG_DBG("Checking prefix %s", prefixes[p]);

                if (fileInfo.fileName().toLower().startsWith(QString(prefixes[p]).toLower()))
                {
                    LOG_DBG("Prefix %s suits", prefixes[p]);
                    return true;
                }
            }
        }
    }

    DB_vfs_t **vfsplugins = m_api->plug_get_vfs_list();

    for (int i = 0; vfsplugins[i]; i++)
    {
        LOG_DBG("Checking vfs plugin %d (%s)", i, vfsplugins[i]->plugin.name ? vfsplugins[i]->plugin.name :
                                                                               "<unknown>");

        if (vfsplugins[i]->is_container &&
            vfsplugins[i]->is_container(filename.toUtf8().data()))
        {
            LOG_DBG("Vfs plugin %d accepted filename", i);
            return true;
        }
    }

    LOG_DBG("Format is not supported");

    return false;
}

QStringList CDdbApi::getSupportedPlaylistExtensions()
{
    LOG_DBG_FUNC();

    QStringList extensions;

    DB_playlist_t **playlistPlugs = m_api->plug_get_playlist_list();

    for (int p = 0; playlistPlugs[p]; p++)
    {
        LOG_DBG("Checking playlist plugin %d (%s)",
                p,
                playlistPlugs[p]->plugin.name ? playlistPlugs[p]->plugin.name :
                                                "<unknown>");

        if (playlistPlugs[p]->extensions && playlistPlugs[p]->load)
        {
            const char **exts = playlistPlugs[p]->extensions;

            for (int e = 0; exts[e]; e++)
            {
                LOG_DBG("Appending extension %s", exts[e]);
                extensions.append(exts[e]);
            }
        }
    }

    extensions.append("dbpl");

    return extensions;
}

QString CDdbApi::version()
{
    return QString::number(VERSION_MAJOR) + "." + QString::number(VERSION_MINOR);
}

QString CDdbApi::getInstallPrefix()
{
    return m_api->get_system_dir(DDB_SYS_DIR_PREFIX);
}

void CDdbApi::terminate()
{
    LOG_DBG("Terminating");

    // FIXME: Is this needed?
    m_api->pl_save_all();
    m_api->sendmessage(DB_EV_TERMINATE, 0, 0, 0);
}

void CDdbApi::acquire(QObject *object)
{
    LOG_DBG("Aquiring object %p", object);
    QQmlEngine::setObjectOwnership(object, QQmlEngine::CppOwnership);
}

void CDdbApi::release(QObject *object)
{
    LOG_DBG("Releasing object %p", object);
    QQmlEngine::setObjectOwnership(object, QQmlEngine::JavaScriptOwnership);
}

void CDdbApi::onPlaylistSwitched()
{
    LOG_DBG_FUNC();

    // When playlist was switched, index of currently playing track in model
    // should be reset to -1 because it resides in old playlist.
    // FIXME: Rewrite handling of current track more elegantly
    m_playitemsModel->onTrackChanged(getPlayingItemIdx());

    m_playitemsModel->onPlaylistChanged();
    m_playlistsModel->onPlaylistSwitched();
}

int CDdbApi::getPlayingItemIdx()
{
    int playingItemIdx = -1;

    m_api->pl_lock();

    DB_playItem_t *it = m_api->streamer_get_playing_track();

    if (it)
    {
        ddb_playlist_t *plt = m_api->plt_get_curr();

        playingItemIdx = m_api->plt_get_item_idx(plt, it, PL_MAIN);

        m_api->pl_item_unref(it);
    }

    m_api->pl_unlock();

    LOG_DBG("Queried plaingItemIdx %d", playingItemIdx);

    return playingItemIdx;
}

void CDdbApi::updatePlaybackDurationMs()
{
    LOG_DBG_FUNC();

    m_playbackDurationMs = 0.0;

    ddb_playItem_t *it = m_api->streamer_get_playing_track();

    if (it)
    {
        float durationSec = m_api->pl_get_item_duration(it);

        LOG_DBG("durationSec: %f", durationSec);

        if (durationSec >= 0)
            m_playbackDurationMs = durationSec * 1000.0;
        else
            m_playbackDurationMs = 0.0;

        m_api->pl_item_unref(it);
    }

    emit playbackDurationMsChanged();
}

void CDdbApi::updatePlaybackOrder()
{
    // FIXME: Backward compatibility?
    EPlaybackOrder newPlaybackOrder =
            (EPlaybackOrder)m_api->conf_get_int("playback.order", PLAYBACK_ORDER_LINEAR);

    if (m_playbackOrder != newPlaybackOrder)
    {
        LOG_DBG("Switched playback order to %d", newPlaybackOrder);
        m_playbackOrder = newPlaybackOrder;
        emit playbackOrderChanged();
    }
}

void CDdbApi::updatePlaybackMode()
{
    EPlaybackMode newPlaybackMode =
            (EPlaybackMode)m_api->conf_get_int("playback.loop", PLAYBACK_MODE_LOOP_ALL);

    if (m_playbackMode != newPlaybackMode)
    {
        LOG_DBG("Switched playback mode to %d", newPlaybackMode);
        m_playbackMode = newPlaybackMode;
        emit playbackModeChanged();
    }
}

void CDdbApi::onPlaylistContentChanged()
{
    LOG_DBG_FUNC();

    m_playitemsModel->onPlaylistChanged();

    // Currently playing item could be deleted from playlist and we need to invalidate
    // it's index after rebuilding playlist content
    m_playitemsModel->onTrackChanged(getPlayingItemIdx());
}

void CDdbApi::onPlayqueueChanged()
{
    LOG_DBG_FUNC();

    m_playitemsModel->onPlayqueueChanged();
}

void CDdbApi::onSongChanged(ddb_playItem_t *from, ddb_playItem_t *to)
{
    LOG_DBG_FUNC("from=%p, to=%p", from, to);

    if (from == NULL && to != NULL)
    {
        // Playback just started, request audio resource
        // FIXME: Handle acquisition error?
        m_audioResource.acquire();
    }
    else if (from != NULL && to == NULL)
    {
        // Playback just stopped, release audio resource
        m_audioResource.release();
    }

    if (from)
        m_api->pl_item_unref(from);

    if (to)
        m_api->pl_item_unref(to);

    m_playitemsModel->onTrackChanged(getPlayingItemIdx());
}

void CDdbApi::onSongStarted()
{
    LOG_DBG("Song started");
    updatePlaybackDurationMs();
    m_playbackState = PlaybackPlaying;
    emit playbackStateChanged();
}

void CDdbApi::onSongFinished()
{
    LOG_DBG("Song finished");
    m_playbackState = PlaybackStopped;
    emit playbackStateChanged();
}

void CDdbApi::onPaused()
{
    if (m_playbackState == PlaybackPlaying)
    {
        LOG_DBG("Paused");
        m_playbackState = PlaybackPaused;
    }
    else
    {
        LOG_DBG("Playing");
        m_playbackState = PlaybackPlaying;
    }

    emit playbackStateChanged();
}

void CDdbApi::onConfigChanged()
{
    LOG_DBG_FUNC();

    updatePlaybackOrder();
    updatePlaybackMode();
}

void CDdbApi::onPluginsLoaded()
{
    LOG_DBG_FUNC();

    // For now, we support only PulseAudio output plugin.
    // If another plugin is selected, switch back to supported one.
    // FIXME: Decide how to handle different output plugins
    if (confGetStr("output_plugin") != "PulseAudio output plugin")
    {
        confSetStr("output_plugin", "PulseAudio output plugin");
        m_api->sendmessage(DB_EV_REINIT_SOUND, 0, 0, 0);
    }

    m_equalizerModel->onPluginsLoaded();
}

void CDdbApi::onDspChainChanged()
{
    LOG_DBG("DSP chain changed");
    m_equalizerModel->onDspChainChanged();
}

void CDdbApi::onPlaybackStateChanged()
{
    LOG_DBG_FUNC("playbackState()=%d", playbackState());

    // FIXME: pass state as argument?
    if (playbackState() == PlaybackPlaying)
    {
        m_playposUpdateTimer.start();
        onPlayposUpdate();
    }
    else if (playbackState() == PlaybackPaused)
    {
        m_playposUpdateTimer.stop();
        onPlayposUpdate();
    }
    else
    {
        m_playposUpdateTimer.stop();
        m_playbackPositionMs = 0.0;
        emit playbackPositionMsChanged();
    }
}

void CDdbApi::onPlayposUpdate()
{
    // FIXME: Add logs when logging become non-blocking?
    m_playbackPositionMs = m_api->playback_get_pos() / 100.0 * m_playbackDurationMs;
    emit playbackPositionMsChanged();
}

CPluginsModel::CPluginsModel(DB_functions_t *api):
    m_api(api)
{
}

int CPluginsModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return m_plugins.count();
}

QVariant CPluginsModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() ||
        index.row() < 0 ||
        index.row() >= m_plugins.count())
        return QVariant();

    if (role == PtrRole)
        return m_plugins[index.row()].ptr;
    else if (role == PlugIdRole)
        return m_plugins[index.row()].plugId;
    else if (role == NameRole)
        return m_plugins[index.row()].name;
    else if (role == DescriptionRole)
        return m_plugins[index.row()].description;
    else if (role == CopyrightRole)
        return m_plugins[index.row()].copyright;
    else if (role == WebsiteRole)
        return m_plugins[index.row()].website;
    else if (role == VersionMajorRole)
        return m_plugins[index.row()].versionMajor;
    else if (role == VersionMinorRole)
        return m_plugins[index.row()].versionMinor;
    else if (role == HasSettingsRole)
        return m_plugins[index.row()].hasSettings;

    return QVariant();
}

QHash<int, QByteArray> CPluginsModel::roleNames() const
{
    QHash<int, QByteArray> roles;

    roles[PtrRole] = "ptr";
    roles[PlugIdRole] = "plugId";
    roles[NameRole] = "name";
    roles[DescriptionRole] = "description";
    roles[CopyrightRole] = "copyright";
    roles[WebsiteRole] = "website";
    roles[VersionMajorRole] = "versionMajor";
    roles[VersionMinorRole] = "versionMinor";
    roles[HasSettingsRole] = "hasSettings";

    return roles;
}

void CPluginsModel::refreshPlugins()
{
    LOG_DBG_FUNC();

    beginResetModel();

    m_plugins.clear();

    DB_plugin_t **plugins = m_api->plug_get_list();

    for (int p = 0; plugins[p]; p++)
    {
        SPluginRecord record;

        LOG_DBG("Found plugin %s (%s) version %d.%d",
                plugins[p]->id,
                plugins[p]->name,
                plugins[p]->version_major,
                plugins[p]->version_minor);

        record.ptr = PtrToQString(plugins[p]);
        record.plugId = QString::fromUtf8(plugins[p]->id);
        record.name = QString::fromUtf8(plugins[p]->name);
        record.description = QString::fromUtf8(plugins[p]->descr);
        record.copyright = QString::fromUtf8(plugins[p]->copyright);
        record.website = QString::fromUtf8(plugins[p]->website);
        record.versionMajor = plugins[p]->version_major;
        record.versionMinor = plugins[p]->version_minor;
        record.hasSettings = plugins[p]->configdialog != NULL;

        m_plugins.append(record);
    }

    endResetModel();
}

bool CEqualizerModel::available()
{
    return getEq() != NULL;
}

bool CEqualizerModel::enabled()
{
    ddb_dsp_context_t *eq = getEq();
    return eq && eq->enabled;
}

float CEqualizerModel::getParam(EEqualizerParams param)
{
    float value = 0.0;

    ddb_dsp_context_t *eq = getEq();

    if (eq)
    {
        char strVal[20];

        eq->plugin->get_param(eq, param, strVal, sizeof(strVal));

        value = atof(strVal);
    }

    return value;
}

void CEqualizerModel::setParam(EEqualizerParams param, float value)
{
    LOG_DBG("Setting eq param %d to %f", param, value);

    ddb_dsp_context_t *eq = getEq();

    if (eq)
    {
        char strVal[100];
        snprintf(strVal, sizeof(strVal), "%f", value);

        eq->plugin->set_param(eq, param, strVal);

        m_api->streamer_dsp_chain_save();
    }
}

float CEqualizerModel::preamp()
{
    return getParam(PARAM_PREAMP);
}

float CEqualizerModel::band55Hz()
{
    return getParam(PARAM_BAND_55HZ);
}

float CEqualizerModel::band77Hz()
{
    return getParam(PARAM_BAND_77HZ);
}

float CEqualizerModel::band110Hz()
{
    return getParam(PARAM_BAND_110HZ);
}

float CEqualizerModel::band156Hz()
{
    return getParam(PARAM_BAND_156HZ);
}

float CEqualizerModel::band220Hz()
{
    return getParam(PARAM_BAND_220HZ);
}

float CEqualizerModel::band311Hz()
{
    return getParam(PARAM_BAND_311HZ);
}

float CEqualizerModel::band440Hz()
{
    return getParam(PARAM_BAND_440HZ);
}

float CEqualizerModel::band622Hz()
{
    return getParam(PARAM_BAND_622HZ);
}

float CEqualizerModel::band880Hz()
{
    return getParam(PARAM_BAND_880HZ);
}

float CEqualizerModel::band1200Hz()
{
    return getParam(PARAM_BAND_1200HZ);
}

float CEqualizerModel::band1800Hz()
{
    return getParam(PARAM_BAND_1800HZ);
}

float CEqualizerModel::band2500Hz()
{
    return getParam(PARAM_BAND_2500HZ);
}

float CEqualizerModel::band3500Hz()
{
    return getParam(PARAM_BAND_3500HZ);
}

float CEqualizerModel::band5000Hz()
{
    return getParam(PARAM_BAND_5000HZ);
}

float CEqualizerModel::band7000Hz()
{
    return getParam(PARAM_BAND_7000HZ);
}

float CEqualizerModel::band10000Hz()
{
    return getParam(PARAM_BAND_10000HZ);
}

float CEqualizerModel::band14000Hz()
{
    return getParam(PARAM_BAND_14000HZ);
}

float CEqualizerModel::band20000Hz()
{
    return getParam(PARAM_BAND_20000HZ);
}

void CEqualizerModel::setEnabled(bool enabled)
{
    LOG_DBG("%s", enabled ? "Enabling eq" : "Disabling eq");

    ddb_dsp_context_t *eq = getEq();

    if (eq)
    {
        eq->enabled = enabled ? 1 : 0;

        m_api->streamer_dsp_refresh();
        m_api->streamer_dsp_chain_save();

        emit enabledChanged();
    }
}

void CEqualizerModel::setPreamp(float value)
{
    setParam(PARAM_PREAMP, value);
    emit preampChanged();
}

void CEqualizerModel::setBand55Hz(float value)
{
    setParam(PARAM_BAND_55HZ, value);
    emit band55HzChanged();
}

void CEqualizerModel::setBand77Hz(float value)
{
    setParam(PARAM_BAND_77HZ, value);
    emit band77HzChanged();
}

void CEqualizerModel::setBand110Hz(float value)
{
    setParam(PARAM_BAND_110HZ, value);
    emit band110HzChanged();
}

void CEqualizerModel::setBand156Hz(float value)
{
    setParam(PARAM_BAND_156HZ, value);
    emit band156HzChanged();
}

void CEqualizerModel::setBand220Hz(float value)
{
    setParam(PARAM_BAND_220HZ, value);
    emit band220HzChanged();
}

void CEqualizerModel::setBand311Hz(float value)
{
    setParam(PARAM_BAND_311HZ, value);
    emit band311HzChanged();
}

void CEqualizerModel::setBand440Hz(float value)
{
    setParam(PARAM_BAND_440HZ, value);
    emit band440HzChanged();
}

void CEqualizerModel::setBand622Hz(float value)
{
    setParam(PARAM_BAND_622HZ, value);
    emit band622HzChanged();
}

void CEqualizerModel::setBand880Hz(float value)
{
    setParam(PARAM_BAND_880HZ, value);
    emit band880HzChanged();
}

void CEqualizerModel::setBand1200Hz(float value)
{
    setParam(PARAM_BAND_1200HZ, value);
    emit band1200HzChanged();
}

void CEqualizerModel::setBand1800Hz(float value)
{
    setParam(PARAM_BAND_1800HZ, value);
    emit band1800HzChanged();
}

void CEqualizerModel::setBand2500Hz(float value)
{
    setParam(PARAM_BAND_2500HZ, value);
    emit band2500HzChanged();
}

void CEqualizerModel::setBand3500Hz(float value)
{
    setParam(PARAM_BAND_3500HZ, value);
    emit band3500HzChanged();
}

void CEqualizerModel::setBand5000Hz(float value)
{
    setParam(PARAM_BAND_5000HZ, value);
    emit band5000HzChanged();
}

void CEqualizerModel::setBand7000Hz(float value)
{
    setParam(PARAM_BAND_7000HZ, value);
    emit band7000HzChanged();
}

void CEqualizerModel::setBand10000Hz(float value)
{
    setParam(PARAM_BAND_10000HZ, value);
    emit band10000HzChanged();
}

void CEqualizerModel::setBand14000Hz(float value)
{
    setParam(PARAM_BAND_14000HZ, value);
    emit band14000HzChanged();
}

void CEqualizerModel::setBand20000Hz(float value)
{
    setParam(PARAM_BAND_20000HZ, value);
    emit band20000HzChanged();
}

CEqualizerModel::CEqualizerModel(DB_functions_t *api):
    m_api(api)
{
    LOG_DBG_FUNC("%p", this);
}

CEqualizerModel::~CEqualizerModel()
{
    LOG_DBG_FUNC("%p", this);
}

void CEqualizerModel::onPluginsLoaded()
{
    notifyAllPropertiesChanged();
}

void CEqualizerModel::onDspChainChanged()
{
    notifyAllPropertiesChanged();
}

// FIXME: This looks non-threadsafe
ddb_dsp_context_t* CEqualizerModel::getEq()
{
    ddb_dsp_context_t* dsp = m_api->streamer_get_dsp_chain();

    while (dsp)
    {
        if (strcmp(dsp->plugin->plugin.id, "supereq") == 0)
        {
            return dsp;
        }

        dsp = dsp->next;
    }

    return NULL;
}

void CEqualizerModel::notifyAllPropertiesChanged()
{
    emit availableChanged();
    emit enabledChanged();

    emit preampChanged();
    emit band55HzChanged();
    emit band77HzChanged();
    emit band110HzChanged();
    emit band156HzChanged();
    emit band220HzChanged();
    emit band311HzChanged();
    emit band440HzChanged();
    emit band622HzChanged();
    emit band880HzChanged();
    emit band1200HzChanged();
    emit band1800HzChanged();
    emit band2500HzChanged();
    emit band3500HzChanged();
    emit band5000HzChanged();
    emit band7000HzChanged();
    emit band10000HzChanged();
    emit band14000HzChanged();
    emit band20000HzChanged();
}
