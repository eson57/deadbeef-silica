#ifndef __TRACKMETADATAMODEL_H
#define __TRACKMETADATAMODEL_H

#include <deadbeef/deadbeef.h>

#include <QAbstractListModel>

class CTrackMetadataModel : public QAbstractListModel
{
    Q_OBJECT

    Q_PROPERTY(int idx READ idx WRITE setIdx NOTIFY idxChanged)

public:
    enum
    {
        KeyRole = Qt::UserRole,
        KeyTitleRole,
        ValueRole
    };

    CTrackMetadataModel(DB_functions_t *api, bool provideProperties);
    ~CTrackMetadataModel();

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;

    int idx();
    void setIdx(int idx);

signals:
    void idxChanged();

protected:
    QHash<int, QByteArray> roleNames() const;

private:
    struct SMetaRecord {
        QString key;
        QString keyTitle;
        QString value;
    };

    void updateKeys();

    DB_functions_t *m_api;
    bool m_provideProperties;

    int m_idx;
    QVector<SMetaRecord> m_keys;
};

#endif // CTRACKMETADATAMODEL_H
