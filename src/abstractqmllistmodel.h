#ifndef __ABSTRACTQMLLISTMODEL_H
#define __ABSTRACTQMLLISTMODEL_H

#include <QAbstractListModel>

class CAbstractQmlListModel : public QAbstractListModel
{
    Q_OBJECT

    Q_PROPERTY(int count READ rowCount NOTIFY countChanged)

public:
    explicit CAbstractQmlListModel(QObject *parent = NULL);

    Q_INVOKABLE QVariantMap get(int row);

    Q_INVOKABLE bool setProperty(int row, QString roleName, QVariant value);

signals:
    void countChanged();
};

#endif // __ABSTRACTQMLLISTMODEL_H
