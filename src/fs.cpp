#include "fs.h"

#include <QDir>

#include <unistd.h>

CFs::CFs(QObject *parent) : QObject(parent)
{
}

bool CFs::pathExists(QString path)
{
    QFileInfo fileInfo(path);

    if (!fileInfo.exists())
        return false;

    return true;
}

bool CFs::isDir(QString path)
{
    QFileInfo fileInfo(path);

    if (!fileInfo.isDir())
        return false;

    return true;
}

QString CFs::getAbsoluteOrCurrent(QString path)
{
    QDir dir(path);

    if (!dir.exists())
        dir = QDir::current();

    return dir.absolutePath();
}
