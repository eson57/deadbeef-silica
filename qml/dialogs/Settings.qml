import QtQuick 2.0
import Sailfish.Silica 1.0

import deadbeef 1.0

Page {
    SilicaFlickable {
        anchors.fill: parent

        contentHeight: settingsColumn.height + Theme.paddingLarge

        Column {
            id: settingsColumn

            width: parent.width

            PageHeader {
                title: qsTr("Settings")
            }

            SectionHeader {
                text: qsTr("Playback options")
            }

            ComboBox {
                label: qsTr("Looping")

                currentItem: DdbApi.playbackMode === DdbApi.LoopNone    ? loopNone :
                             DdbApi.playbackMode === DdbApi.LoopSingle  ? loopSingle :
                                                                          loopAll

                menu: ContextMenu {
                    MenuItem {
                        id: loopNone
                        text: qsTr("Don't loop")
                        onClicked: DdbApi.playbackMode = DdbApi.LoopNone
                    }

                    MenuItem {
                        id: loopSingle
                        text: qsTr("Loop single song")
                        onClicked: DdbApi.playbackMode = DdbApi.LoopSingle
                    }

                    MenuItem {
                        id: loopAll
                        text: qsTr("Loop all")
                        onClicked: DdbApi.playbackMode = DdbApi.LoopAll
                    }
                }
            }

            ComboBox {
                label: qsTr("Playback order")

                currentItem: DdbApi.playbackOrder === DdbApi.OrderLinear        ? orderLinear :
                             DdbApi.playbackOrder === DdbApi.OrderShuffleTracks ? orderShuffleTracks :
                             DdbApi.playbackOrder === DdbApi.OrderShuffleAlbums ? orderShuffleAlbums :
                                                                                  orderRandom

                menu: ContextMenu {
                    MenuItem {
                        id: orderLinear
                        text: qsTr("Linear")
                        onClicked: DdbApi.playbackOrder = DdbApi.OrderLinear
                    }

                    MenuItem {
                        id: orderShuffleTracks
                        text: qsTr("Shuffle tracks")
                        onClicked: DdbApi.playbackOrder = DdbApi.OrderShuffleTracks
                    }

                    MenuItem {
                        id: orderShuffleAlbums
                        text: qsTr("Shuffle albums")
                        onClicked: DdbApi.playbackOrder = DdbApi.OrderShuffleAlbums
                    }

                    MenuItem {
                        id: orderRandom
                        text: qsTr("Random")
                        onClicked: DdbApi.playbackOrder = DdbApi.OrderRandom
                    }

                }
            }

            Item {
                width: parent.width
                height: Theme.paddingLarge
            }

            ButtonLayout {
                Button {
                    function findPluginByName(pluginName) {
                        for (var i = 0; i < DdbApi.pluginsModel.count; i++) {
                            var plugin = DdbApi.pluginsModel.get(i);

                            if (plugin.name === pluginName)
                                return plugin;
                        }

                        return null;
                    }

                    property string lastFmPluginName: "last.fm scrobbler"

                    enabled: {
                        var plugin = findPluginByName(lastFmPluginName);
                        return plugin !== null && plugin.hasSettings;
                    }

                    visible: enabled

                    text: qsTr("Setup Last.fm")
                    onClicked: pageStack.push(Qt.resolvedUrl("../dialogs/PluginSettings.qml"),
                                              {
                                                  ptr: findPluginByName(lastFmPluginName).ptr
                                              })
                }

                Button {
                    ButtonLayout.newLine: true

                    text: qsTr("Plugins")
                    onClicked: pageStack.push(Qt.resolvedUrl("../dialogs/Plugins.qml"))
                }
            }
        }

        VerticalScrollDecorator {}
    }
}
