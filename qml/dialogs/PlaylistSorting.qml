import QtQuick 2.0
import Sailfish.Silica 1.0

import deadbeef 1.0

Page {
    PageHeader {
        id: header

        anchors {
            left: parent.left
            right: parent.right
            top: parent.top
        }

        title: qsTr("Sort by")
    }

    TextSwitch {
        id: orderSwitch

        anchors {
            left: parent.left
            right: parent.right
            top: header.bottom
            topMargin: Theme.paddingLarge
        }

        text: qsTr("Reverse order")
    }

    SilicaListView {
        anchors {
            left: parent.left
            right: parent.right
            top: orderSwitch.bottom
            topMargin: Theme.paddingLarge
            bottom: parent.bottom
        }

        model: ListModel {
            id: sortOptionsModel

            property var labels: {
                "%artist% %album% %tracknumber%": qsTr("Artist/Album/Track number"),
                "%artist% %album%": qsTr("Artist/Album"),
                "%title%": qsTr("Title"),
                "%tracknumber%": qsTr("Track number"),
                "%album%": qsTr("Album"),
                "%artist%": qsTr("Artist"),
                "%album artist%": qsTr("Album artist"),
                "%date%": qsTr("Date"),
                "%filename%": qsTr("Filename")
            }

            ListElement {
                format: "%artist% %album% %tracknumber%"
            }

            ListElement {
                format: "%artist% %album%"
            }

            ListElement {
                format: "%title%"
            }

            ListElement {
                format: "%tracknumber%"
            }

            ListElement {
                format: "%album%"
            }

            ListElement {
                format: "%artist%"
            }

            ListElement {
                format: "%album artist%"
            }

            ListElement {
                format: "%date%"
            }

            ListElement {
                format: "%filename%"
            }
        }

        delegate: ListItem {
            id: listItem

            Label {
                anchors {
                    left: parent.left
                    leftMargin: Theme.horizontalPageMargin
                    right: parent.right
                    rightMargin: Theme.horizontalPageMargin
                    verticalCenter: parent.verticalCenter
                }

                text: sortOptionsModel.labels[format]
                color: listItem.highlighted ? Theme.highlightColor :
                                              Theme.primaryColor
            }

            onClicked: {
                DdbApi.playitemsModel.sortByTf(format,
                                               orderSwitch.checked ? PlayitemsModel.SortDescending :
                                                                     PlayitemsModel.SortAscending);
                pageStack.pop();
            }
        }

        VerticalScrollDecorator {}
    }
}
