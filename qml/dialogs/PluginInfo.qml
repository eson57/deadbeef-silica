import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    property string ptr
    property string name
    property string description
    property string copyright
    property string website
    property int versionMajor
    property int versionMinor
    property bool hasSettings

    SilicaFlickable {
        anchors.fill: parent
        contentHeight: pluginInfoColumn.height + Theme.paddingLarge

        Column {
            id: pluginInfoColumn
            width: parent.width

            spacing: Theme.paddingLarge

            PageHeader {
                title: name
            }

            SectionHeader {
                text: qsTr("Description")
            }

            LinkedLabel {
                anchors {
                    left: parent.left
                    leftMargin: Theme.horizontalPageMargin
                    right: parent.right
                    rightMargin: Theme.horizontalPageMargin
                }

                plainText: qsTr(description)
                color: Theme.highlightColor
                wrapMode: Text.Wrap
            }

            Label {
                anchors {
                    left: parent.left
                    leftMargin: Theme.horizontalPageMargin
                    right: parent.right
                    rightMargin: Theme.horizontalPageMargin
                }

                text: qsTr("Version") + ": " + versionMajor + "." + versionMinor
                color: Theme.highlightColor
                wrapMode: Text.Wrap
            }

            SectionHeader {
                text: qsTr("Website")
            }

            LinkedLabel {
                anchors {
                    left: parent.left
                    leftMargin: Theme.horizontalPageMargin
                    right: parent.right
                    rightMargin: Theme.horizontalPageMargin
                }

                plainText: website
                color: Theme.highlightColor
                wrapMode: Text.Wrap
            }

            Item {
                width: parent.width
                height: Theme.paddingLarge
            }

            ButtonLayout {
                Button {
                    text: qsTr("Settings")
                    enabled: hasSettings
                    onClicked: pageStack.push(Qt.resolvedUrl("../dialogs/PluginSettings.qml"), {
                                                  ptr: ptr
                                              })
                }

                Button {
                    text: qsTr("Copyright")
                    onClicked: pageStack.push(Qt.resolvedUrl("../dialogs/PluginCopyright.qml"), {
                                                  name: name,
                                                  copyright: copyright
                                              })
                }
            }
        }

        VerticalScrollDecorator {}
    }
}
