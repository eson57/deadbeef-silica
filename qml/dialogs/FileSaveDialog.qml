import QtQuick 2.0
import Sailfish.Silica 1.0

// FIXME: Refactor Fs into separate module
import deadbeef 1.0

import "../items"

Dialog {
    id: fileSaveDialog

    property string path: ""
    property string proposedName: "untitled"

    property alias title: dialogTitle.text

    property string resultPath: path + "/" + fileNameEdit.text + (!hasExtension(fileNameEdit.text) ? _selectedExt : "")

    function installExt(name, ext) {
        extensionsModel.append({
            name: name,
            ext: ext
        });
    }

    // FIXME: Add removeExt()?

    ListModel {
        id: extensionsModel
    }

    // FIXME: What if 'path' is not valid?
    property bool _resultPathValid: fileNameEdit.text.length !== 0

    property string _selectedExt: extensionsModel.count !== 0 ? "." + extensionsModel.get(extensionsComboBox.currentIndex).ext : ""

    canAccept: _resultPathValid && (!Fs.pathExists(resultPath) || (Fs.pathExists(resultPath) && !Fs.isDir(resultPath) && overwriteSwitch.checked))

    Component.onCompleted: {
        fileNameEdit.text = proposedName;
        filterPath();
    }

    onPathChanged: {
        filterPath();
    }

    onProposedNameChanged: {
        if (fileNameEdit.text.length != 0)
            fileNameEdit.text = proposedName;
    }

    onResultPathChanged: {
        overwriteSwitch.checked = false;
    }

    property bool _updatingPath: false

    function filterPath() {
        if (_updatingPath)
            return;

        _updatingPath = true;

        path = Fs.getAbsoluteOrCurrent(path);

        _updatingPath = false;
    }

    function hasExtension(filename) {
        return filename.indexOf(".") !== -1;
    }

    DialogHeader {
        id: dialogHeader
        spacing: Theme.paddingMedium    // to emulate interval between header and its native title
    }

    DialogTitle {
        id: dialogTitle
        anchors.top: dialogHeader.bottom
    }

    SilicaFlickable {
        anchors {
            top: dialogTitle.bottom
            topMargin: Theme.paddingLarge
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        Column {
            width: parent.width

            // FIXME: Unify with FileSelectionField?
            Item {
                width: parent.width
                height: Math.max(pathText.implicitHeight, pathButton.implicitHeight)

                TextArea {
                    id: pathText
                    anchors.left: parent.left
                    anchors.right: pathButton.left

                    label: qsTr("Location")
                    placeholderText: label

                    text: path

                    readOnly: true
                    focusOnClick: true
                }

                Button {
                    id: pathButton

                    anchors.right: parent.right
                    anchors.rightMargin: Theme.horizontalPageMargin
                    width: height

                    text: "..."

                    onClicked: {
                        var dialog = pageStack.push(Qt.resolvedUrl("./FilePicker.qml"),
                                                    {
                                                        path: path,
                                                        selectFiles: false, // FIXME
                                                        selectDirectories: true,
                                                        title: qsTr("Select file location")
                                                    });

                        dialog.accepted.connect(function() {
                            path = dialog.resultPaths[0];
                        });
                    }
                }
            }

            TextField {
                id: fileNameEdit

                width: parent.width

                label: qsTr("New file name")
                placeholderText: label

                inputMethodHints: Qt.ImhNoAutoUppercase

                focus: true

                EnterKey.enabled: fileSaveDialog.canAccept
                EnterKey.iconSource: "image://theme/icon-m-enter-accept"
                EnterKey.onClicked: fileSaveDialog.accept()
            }

            TextSwitch {
                id: overwriteSwitch

                width: parent.width
                height: visible ? implicitHeight : 0

                text: qsTr("File exists. Overwrite?")
                enabled: visible
                visible: _resultPathValid && Fs.pathExists(resultPath) && !Fs.isDir(resultPath)
            }

            Label {
                anchors {
                    left: parent.left
                    leftMargin: Theme.horizontalPageMargin
                    right: parent.right
                    rightMargin: Theme.horizontalPageMargin
                }

                height: visible ? implicitHeight : 0

                text: qsTr("There is already a directory with such name")
                color: "red"

                wrapMode: Text.WordWrap

                enabled: visible
                visible: _resultPathValid && Fs.pathExists(resultPath) && Fs.isDir(resultPath)
            }

            ComboBox {
                id: extensionsComboBox

                width: parent.width

                visible: extensionsModel.count !== 0

                label: qsTr("Format")

                menu: ContextMenu {
                    Repeater {
                        model: extensionsModel

                        delegate: MenuItem {
                            text: name
                        }
                    }
                }
            }
        }

        VerticalScrollDecorator {}
    }
}
