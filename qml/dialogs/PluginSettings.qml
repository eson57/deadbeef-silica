import QtQuick 2.0
import Sailfish.Silica 1.0

import deadbeef 1.0

import "../items"

Dialog {
    property string ptr

    Component.onCompleted: {
        optionsRepeater.model = DdbApi.createConfigDialogModel();
        optionsRepeater.model.ptr = ptr;

        dialogTitle.text = qsTr("%1 settings").arg(optionsRepeater.model.pluginName);
    }

    onAccepted: {
        optionsRepeater.model.commitChanges();
    }

    onRejected: {
        optionsRepeater.model.discardChanges();
    }

    DialogHeader {
        id: dialogHeader
        spacing: Theme.paddingMedium    // to emulate interval between header and its native title
    }

    DialogTitle {
        id: dialogTitle
        anchors.top: dialogHeader.bottom
    }

    SilicaFlickable {
        anchors {
            top: dialogTitle.bottom
            topMargin: Theme.paddingLarge
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        contentHeight: controlsColumn.height + Theme.paddingLarge

        clip: true

        Column {
            id: controlsColumn

            width: parent.width

            spacing: Theme.paddingSmall

            Repeater {
                id: optionsRepeater

                Component {
                    id: entryComponent

                    TextField {
                        anchors.left: parent.left
                        anchors.right: parent.right

                        placeholderText: label

                        EnterKey.iconSource: "image://theme/icon-m-enter-close"
                        EnterKey.onClicked: focus = false
                    }
                }

                Component {
                    id: passwordComponent

                    PasswordField {
                        anchors.left: parent.left
                        anchors.right: parent.right

                        placeholderText: label
                    }
                }

                Component {
                    id: fileComponent

                    FileSelectionField {
                    }
                }

                Component {
                    id: switchComponent

                    TextSwitch {
                        anchors.left: parent.left
                        anchors.right: parent.right
                    }
                }

                Component {
                    id: hscaleComponent

                    Slider {
                        anchors.left: parent.left
                        anchors.right: parent.right

                        valueText: value
                    }
                }

                Component {
                    id: vscaleComponent

                    // FIXME: Respect property semantics and make it vertical
                    Slider {
                        anchors.left: parent.left
                        anchors.right: parent.right

                        valueText: value
                    }
                }

                Component {
                    id: selectComponent

                    ComboBox {
                        property var items: []

                        menu: ContextMenu {
                            Repeater {
                                model: items

                                delegate: MenuItem {
                                    text: modelData
                                }
                            }
                        }
                    }
                }

                Component {
                    id: unsupportedComponent

                    Label {
                        anchors.left: parent.left
                        anchors.right: parent.right
                        anchors.leftMargin: Theme.horizontalPageMargin
                        anchors.rightMargin: Theme.horizontalPageMargin

                        property string label

                        text: "<Unsupported: " + label + ">"

                        wrapMode: Text.Wrap
                    }
                }

                delegate: Loader {
                    anchors {
                        left: parent.left
                        right: parent.right
                    }

                    sourceComponent: type === ConfigDialogModel.EntryType    ? entryComponent :
                                     type === ConfigDialogModel.PasswordType ? passwordComponent :
                                     type === ConfigDialogModel.FileType     ? fileComponent :
                                     type === ConfigDialogModel.CheckboxType ? switchComponent :
                                     type === ConfigDialogModel.HscaleType   ? hscaleComponent :
                                     type === ConfigDialogModel.VscaleType   ? vscaleComponent :
                                     type === ConfigDialogModel.SelectType   ? selectComponent :
                                                                               unsupportedComponent;

                    onLoaded: {
                        if (type === ConfigDialogModel.CheckboxType)
                        {
                            item.text = qsTr(label);
                            item.checked = value;

                            item.checkedChanged.connect(function () {
                                optionsRepeater.model.setValue(index, item.checked);
                            });
                        }
                        else if (type === ConfigDialogModel.EntryType ||
                                 type === ConfigDialogModel.PasswordType)
                        {
                            item.label = qsTr(label);
                            item.text = value;

                            item.textChanged.connect(function () {
                                optionsRepeater.model.setValue(index, item.text);
                            });
                        }
                        else if (type === ConfigDialogModel.HscaleType ||
                                 type === ConfigDialogModel.VscaleType)
                        {
                            item.label = qsTr(label);
                            item.minimumValue = scaleMin;
                            item.maximumValue = scaleMax;
                            item.stepSize = scaleStep;
                            item.value = value;

                            item.valueChanged.connect(function () {
                                optionsRepeater.model.setValue(index, item.value);
                            });
                        }
                        else if (type === ConfigDialogModel.FileType)
                        {
                            item.label = qsTr(label);
                            item.path = value;

                            item.pathChanged.connect(function () {
                                optionsRepeater.model.setValue(index, item.path);
                            });
                        }
                        else if (type === ConfigDialogModel.SelectType)
                        {
                            item.label = qsTr(label);
                            item.items = selectItems;
                            item.currentIndex = value;

                            item.currentIndexChanged.connect(function () {
                                optionsRepeater.model.setValue(index, item.currentIndex);
                            });
                        }
                        else
                        {
                            item.label = qsTr(label);
                        }
                    }
                }
            }
        }

        VerticalScrollDecorator {}
    }
}
