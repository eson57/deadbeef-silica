import QtQuick 2.0
import Sailfish.Silica 1.0

import "../items"

Dialog {
    id: playlistCreationDialog

    canAccept: playlistNameLabel.textValid

    property alias playlistName: playlistNameLabel.text
    property bool creationMode: false

    DialogHeader {
        id: dialogHeader
        spacing: Theme.paddingMedium    // to emulate interval between header and its native title
    }

    DialogTitle {
        id: dialogTitle
        anchors.top: dialogHeader.bottom
        text: creationMode ? qsTr("Add playlist") : qsTr("Rename playlist")
    }

    Column {
        anchors {
            top: dialogTitle.bottom
            topMargin: Theme.paddingLarge
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        spacing: Theme.paddingSmall

        TextField {
            id: playlistNameLabel

            width: parent.width

            focus: true
            label: qsTr("Playlist name")
            placeholderText: label

            property bool textValid: text.length !== 0

            EnterKey.iconSource: "image://theme/icon-m-enter-accept"
            EnterKey.onClicked: playlistCreationDialog.accept()
            EnterKey.enabled: textValid
        }
    }
}
