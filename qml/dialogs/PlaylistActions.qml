import QtQuick 2.0
import Sailfish.Silica 1.0

import deadbeef 1.0

Page {
    function runMediaFileDialog(onAccepted, title) {
        // FIXME: Why do we need to save reference to DdbApi
        //        instance to avoid referencing 'undefined'?
        var ddbApi = DdbApi;

        var lastOpenedDirectory = ddbApi.confGetStr("filechooser.lastdir", "");

        var dialog = pageStack.replace(Qt.resolvedUrl("../dialogs/FilePicker.qml"),
                                                   {
                                                       path: lastOpenedDirectory,
                                                       selectDirectories: true,
                                                       multiple: true,
                                                       title: arguments.length >= 2 ? title : ""
                                                   });

        var supportedFormatsFilter = function(filename) {
            return ddbApi.isFormatSupported(filename);
        };

        var allFilesFilter = function(filename) {
            return true;
        };

        dialog.installFilter(qsTr("Supported formats"), supportedFormatsFilter);
        dialog.installFilter(qsTr("All files"), allFilesFilter);

        dialog.accepted.connect(function() {
            onAccepted(dialog.resultPaths);
        });

        dialog.done.connect(function() {
            ddbApi.confSetStr("filechooser.lastdir", dialog.path);
        });
    }

    function runSavePlaylistFileDialog(onAccepted, title) {
        var ddbApi = DdbApi;

        var lastOpenedDirectory = ddbApi.confGetStr("filechooser.playlist.lastdir", "");

        var dialog = pageStack.replace(Qt.resolvedUrl("../dialogs/FileSaveDialog.qml"),
                                       {
                                           path: lastOpenedDirectory,
                                           title: arguments.length >= 2 ? title : ""
                                       });

        ddbApi.getSupportedPlaylistExtensions().forEach(function(ext) {
            dialog.installExt(ext, ext);
        });

        dialog.accepted.connect(function() {
            onAccepted(dialog.resultPath);
        });

        dialog.done.connect(function() {
            ddbApi.confSetStr("filechooser.playlist.lastdir", dialog.path);
        });
    }

    SilicaListView {
        anchors.fill: parent

        header: PageHeader {
            title: qsTr("Playlist actions")
        }

        model: ListModel {
            id: actionsModel

            property var labels: {
                "add-files": qsTr("Add file(s)/folder(s)", "playlist actions menu item"),
                "add-location": qsTr("Add location"),
                "save": qsTr("Save playlist", "playlist actions menu item"),
                "sort": qsTr("Sort playlist"),
                "clear": qsTr("Clear playlist")
            }

            property var actions: {
                "add-files": function() {
                    var ddbApi = DdbApi;
                    runMediaFileDialog(function(selectedPaths) {
                        ddbApi.addPaths(selectedPaths);
                    },
                    qsTr("Add file(s)/folder(s)", "file add dialog title"));
                },

                "add-location": function() {
                    var ddbApi = DdbApi;

                    var dialog = pageStack.replace(Qt.resolvedUrl("../dialogs/AddLocation.qml"));

                    dialog.accepted.connect(function() {
                        ddbApi.addLocation(dialog.location);
                    });
                },

                "save": function() {
                    var ddbApi = DdbApi;
                    runSavePlaylistFileDialog(function(newFilePath) {
                        ddbApi.savePlaylist(newFilePath);
                    },
                    qsTr("Save playlist", "save playlist dialog title"));
                },

                "sort": function() {
                    pageStack.replace(Qt.resolvedUrl("../dialogs/PlaylistSorting.qml"));
                },

                "clear": function() {
                    DdbApi.clearPlaylist();
                    pageStack.pop();
                }
            }

            ListElement {
                name: "add-files"
            }

            ListElement {
                name: "add-location"
            }

            ListElement {
                name: "save"
            }

            ListElement {
                name: "sort"
            }

            ListElement {
                name: "clear"
            }
        }

        delegate: ListItem {
            id: listItem

            Label {
                anchors {
                    left: parent.left
                    leftMargin: Theme.horizontalPageMargin
                    right: parent.right
                    rightMargin: Theme.horizontalPageMargin
                    verticalCenter: parent.verticalCenter
                }

                text: actionsModel.labels[name]
                color: listItem.highlighted ? Theme.highlightColor :
                                              Theme.primaryColor
            }

            onClicked: actionsModel.actions[name]()
        }

        VerticalScrollDecorator {}
    }
}
