import QtQuick 2.0
import Sailfish.Silica 1.0

import "../items"

import deadbeef 1.0

Page {
    property int trackIdx: -1

    Component.onCompleted: {
        tagsRepeater.model = DdbApi.createTrackTagsModel();
        tagsRepeater.model.idx = trackIdx;

        propertiesRepeater.model = DdbApi.createTrackPropertiesModel();
        propertiesRepeater.model.idx = trackIdx;
    }

    SilicaFlickable {
        anchors.fill: parent
        contentWidth: parent.width
        contentHeight: metaColumn.height

        Column {
            id: metaColumn
            width: parent.width

            PageHeader { title: qsTr("Track Properties") }

            SectionHeader { text: qsTr("Metadata") }

            TrackPropertiesPlaceholder {
                enabled: tagsRepeater.count == 0
                text: qsTr("No metadata")
            }

            Repeater {
                id: tagsRepeater

                TextField {
                    width: parent.width

                    label: qsTr(keyTitle)
                    placeholderText: label
                    text: value

                    readOnly: true
                    focusOnClick: true
                }
            }

            SectionHeader { text: qsTr("Properties") }

            TrackPropertiesPlaceholder {
                enabled: propertiesRepeater.count == 0
                text: qsTr("No properties")
            }

            Repeater {
                id: propertiesRepeater

                TextField {
                    width: parent.width

                    label: qsTr(keyTitle)
                    placeholderText: label
                    text: value

                    readOnly: true
                    focusOnClick: true
                }
            }
        }

        VerticalScrollDecorator {}
    }
}
