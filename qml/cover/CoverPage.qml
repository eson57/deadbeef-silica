import QtQuick 2.0
import Sailfish.Silica 1.0

import deadbeef 1.0

CoverBackground {
    Loader {
        anchors.fill: parent
        anchors.leftMargin: Theme.paddingMedium
        anchors.rightMargin: Theme.paddingMedium
        anchors.topMargin: Theme.paddingMedium
        anchors.bottomMargin: parent.height / 5 // FIXME: Could it be done more elegant?

        sourceComponent: DdbApi.playbackState === DdbApi.PlaybackStopped ? stoppedItem :
                                                                           nowPlayingItem
    }

    Component {
        id: stoppedItem

        Item {
            Label {
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.verticalCenter: parent.verticalCenter

                text: "DeadBeef"
                horizontalAlignment: Text.AlignHCenter
            }
        }
    }

    Component {
        id: nowPlayingItem

        Item {
            Column {
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.verticalCenter: parent.verticalCenter

                Label {
                    text: DdbApi.playitemsModel.nowPlayingtTitle
                    color: Theme.primaryColor
                    wrapMode: Text.Wrap
                    horizontalAlignment: Text.AlignHCenter

                    width: parent.width
                }

                Label {
                    text: DdbApi.playitemsModel.nowPlayingArtistAlbum
                    color: Theme.secondaryColor
                    wrapMode: Text.Wrap
                    horizontalAlignment: Text.AlignHCenter

                    width: parent.width
                }
            }
        }
    }

    CoverActionList {
        id: coverActions

        CoverAction {
            iconSource: "image://theme/icon-cover-" + (DdbApi.playbackState == DdbApi.PlaybackPlaying ? "pause" :
                                                                                                        "play")
            onTriggered: DdbApi.playPause()
        }

        CoverAction {
            iconSource: "image://theme/icon-cover-next-song"
            onTriggered: DdbApi.next()
        }
    }
}
