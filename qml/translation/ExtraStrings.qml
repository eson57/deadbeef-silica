import QtQuick 2.0

// This file should list text strings that are not directly embedded into QML source code,
// but also should be translated using qsTr(), e.g. text from plugin's configuration dialogs

Component {
    property var trackMetaTitles: [
        QT_TRANSLATE_NOOP("TrackProperties", "Artist"),
        QT_TRANSLATE_NOOP("TrackProperties", "Track Title"),
        QT_TRANSLATE_NOOP("TrackProperties", "Album"),
        QT_TRANSLATE_NOOP("TrackProperties", "Date"),
        QT_TRANSLATE_NOOP("TrackProperties", "Track Number"),
        QT_TRANSLATE_NOOP("TrackProperties", "Total Tracks"),
        QT_TRANSLATE_NOOP("TrackProperties", "Genre"),
        QT_TRANSLATE_NOOP("TrackProperties", "Composer"),
        QT_TRANSLATE_NOOP("TrackProperties", "Disc Number"),
        QT_TRANSLATE_NOOP("TrackProperties", "Total Disks"),
        QT_TRANSLATE_NOOP("TrackProperties", "Comment"),
    ]

    property var trackPropTitles: [
        QT_TRANSLATE_NOOP("TrackProperties", "Location"),
        QT_TRANSLATE_NOOP("TrackProperties", "Subtrack Index"),
        QT_TRANSLATE_NOOP("TrackProperties", "Duration"),
        QT_TRANSLATE_NOOP("TrackProperties", "Tag Type(s)"),
        QT_TRANSLATE_NOOP("TrackProperties", "Embedded Cuesheet"),
        QT_TRANSLATE_NOOP("TrackProperties", "Codec"),
    ]

    property var aac: [
        QT_TRANSLATE_NOOP("PluginInfo", "plays aac files, supports raw aac files, as well as mp4 container"),
    ]

    property var adplug: [
        QT_TRANSLATE_NOOP("PluginInfo", "Adplug player (ADLIB OPL2/OPL3 emulator)"),
        QT_TRANSLATE_NOOP("PluginSettings", "Prefer Ken emu over Satoh (surround won't work)"),
        QT_TRANSLATE_NOOP("PluginSettings", "Enable surround"),
    ]

    property var alac: [
        QT_TRANSLATE_NOOP("PluginInfo", "plays alac files from MP4 and M4A files"),
    ]

    property var ao: [
        QT_TRANSLATE_NOOP("PluginInfo", "plays psf, psf2, spu, ssf, dsf, qsf file formats"),
    ]

    property var dca: [
        QT_TRANSLATE_NOOP("PluginInfo", "plays dts-encoded files using libdca from VLC project"),
    ]

    property var ddb_gui_silica: [
        QT_TRANSLATE_NOOP("PluginInfo", "User interface implemented using Sailfish Silica QML module"),
    ]

    property var dsp_libsrc: [
        QT_TRANSLATE_NOOP("PluginInfo", "High quality samplerate converter using libsamplerate, http://www.mega-nerd.com/SRC/"),
    ]

    property var dumb: [
        QT_TRANSLATE_NOOP("PluginInfo", "module player based on DUMB library"),
        QT_TRANSLATE_NOOP("PluginSettings", "Resampling quality (0..5, higher is better)"),
        QT_TRANSLATE_NOOP("PluginSettings", "8-bit output (default is 16)"),
        QT_TRANSLATE_NOOP("PluginSettings", "Internal DUMB volume (0..128)"),
        QT_TRANSLATE_NOOP("PluginSettings", "Volume ramping (0 is none, 1 is note on/off, 2 is always)"),
    ]

    property var ffap: [
        QT_TRANSLATE_NOOP("PluginInfo", "APE player based on code from libavc and rockbox"),
    ]

    property var ffmpeg: [
        QT_TRANSLATE_NOOP("PluginInfo", "decodes audio formats using FFMPEG libavcodec"),
        QT_TRANSLATE_NOOP("PluginSettings", "Use all extensions supported by ffmpeg"),
        QT_TRANSLATE_NOOP("PluginSettings", "File Extensions (separate with ';')"),
    ]

    property var flac: [
        QT_TRANSLATE_NOOP("PluginInfo", "FLAC decoder using libFLAC"),
    ]

    property var gme: [
        QT_TRANSLATE_NOOP("PluginInfo", "chiptune/game music player based on GME library"),
        QT_TRANSLATE_NOOP("PluginSettings", "Max song length (in minutes)"),
        QT_TRANSLATE_NOOP("PluginSettings", "Fadeout length (seconds)"),
        QT_TRANSLATE_NOOP("PluginSettings", "Play loops nr. of times (if available)"),
    ]

    property var lastfm: [
        QT_TRANSLATE_NOOP("PluginInfo", "Sends played songs information to your last.fm account, or other service that use AudioScrobbler protocol"),
        QT_TRANSLATE_NOOP("PluginSettings", "Enable scrobbler"),
        QT_TRANSLATE_NOOP("PluginSettings", "Disable nowplaying"),
        QT_TRANSLATE_NOOP("PluginSettings", "Username"),
        QT_TRANSLATE_NOOP("PluginSettings", "Password"),
        QT_TRANSLATE_NOOP("PluginSettings", "Scrobble URL"),
        QT_TRANSLATE_NOOP("PluginSettings", "Prefer Album Artist over Artist field"),
        QT_TRANSLATE_NOOP("PluginSettings", "Send MusicBrainz ID"),
        QT_TRANSLATE_NOOP("PluginSettings", "Submit tracks shorter than 30 seconds (not recommended)"),
    ]

    property var m3u: [
        QT_TRANSLATE_NOOP("PluginInfo", "Importing and exporting M3U and PLS formats\nRecognizes .pls, .m3u and .m3u8 file types\n\nNOTE: only utf8 file names are currently supported"),
    ]

    property var mms: [
        QT_TRANSLATE_NOOP("PluginInfo", "MMS streaming plugin based on libmms"),
    ]

    property var mono2stereo: [
        QT_TRANSLATE_NOOP("PluginInfo", "Mono to stereo converter DSP"),
    ]

    property var mp3: [
        QT_TRANSLATE_NOOP("PluginInfo", "MPEG v1/2 layer1/2/3 decoder\n\nUsing libmpg123 backend.\n"),
        QT_TRANSLATE_NOOP("PluginSettings", "Disable gapless playback (faster scanning)"),
        QT_TRANSLATE_NOOP("PluginSettings", "Backend"),
    ]

    property var mpris: [
        QT_TRANSLATE_NOOP("PluginInfo", "Communicate with other applications using D-Bus."),
    ]

    property var musepack: [
        QT_TRANSLATE_NOOP("PluginInfo", "Musepack decoder using libmppdec"),
    ]

    property var nullout: [
        QT_TRANSLATE_NOOP("PluginInfo", "This plugin takes the audio data, and discards it,\nso nothing will play.\nThis is useful for testing."),
    ]

    property var pulse: [
        QT_TRANSLATE_NOOP("PluginInfo", "At the moment of this writing, PulseAudio seems to be very unstable in many (or most) GNU/Linux distributions.\nIf you experience problems - please try switching to ALSA or OSS output.\nIf that doesn't help - please uninstall PulseAudio from your system, and try ALSA or OSS again.\nThanks for understanding"),
        QT_TRANSLATE_NOOP("PluginSettings", "PulseAudio server"),
        QT_TRANSLATE_NOOP("PluginSettings", "Preferred buffer size"),
    ]

    property var sc86: [
        QT_TRANSLATE_NOOP("PluginInfo", "SC68 player (Atari ST SNDH YM2149)"),
        QT_TRANSLATE_NOOP("PluginSettings", "Default song length (in minutes)"),
        QT_TRANSLATE_NOOP("PluginSettings", "Samplerate"),
        QT_TRANSLATE_NOOP("PluginSettings", "Skip when shorter than (sec)"),
    ]

    property var shn: [
        QT_TRANSLATE_NOOP("PluginInfo", "decodes shn files"),
        QT_TRANSLATE_NOOP("PluginSettings", "Relative seek table path"),
        QT_TRANSLATE_NOOP("PluginSettings", "Absolute seek table path"),
        QT_TRANSLATE_NOOP("PluginSettings", "Swap audio bytes (toggle if all you hear is static)"),
    ]

    property var sid: [
        QT_TRANSLATE_NOOP("PluginInfo", "SID player based on libsidplay2"),
        QT_TRANSLATE_NOOP("PluginSettings", "Enable HVSC Songlength DB"),
        QT_TRANSLATE_NOOP("PluginSettings", "Songlengths.txt (from HVSC)"),
        QT_TRANSLATE_NOOP("PluginSettings", "Samplerate"),
        QT_TRANSLATE_NOOP("PluginSettings", "Bits per sample (8 or 16)"),
        QT_TRANSLATE_NOOP("PluginSettings", "Mono synth"),
        QT_TRANSLATE_NOOP("PluginSettings", "Default song length (sec)"),
    ]

    property var sndfile: [
        QT_TRANSLATE_NOOP("PluginInfo", "wav/aiff player using libsndfile"),
        QT_TRANSLATE_NOOP("PluginSettings", "File Extensions (separate with ';')"),
    ]

    property var supereq: [
        QT_TRANSLATE_NOOP("PluginInfo", "equalizer plugin using SuperEQ library"),
    ]

    property var tta: [
        QT_TRANSLATE_NOOP("PluginInfo", "tta decoder based on TTA Hardware Players Library Version 1.2"),
    ]

    property var vfs_curl: [
        QT_TRANSLATE_NOOP("PluginInfo", "http and ftp streaming module using libcurl, with ICY protocol support"),
        QT_TRANSLATE_NOOP("PluginSettings", "Emulate track change events (for scrobbling)"),
    ]

    property var vfs_sdio: [
        QT_TRANSLATE_NOOP("PluginInfo", "Standard IO plugin\nUsed for reading normal local files\nIt is statically linked, so you can't delete it."),
    ]

    property var vfs_zip: [
        QT_TRANSLATE_NOOP("PluginInfo", "play files directly from zip files"),
    ]

    property var vorbis: [
        QT_TRANSLATE_NOOP("PluginInfo", "OggVorbis decoder using standard xiph.org libraries"),
    ]

    property var vtx: [
        QT_TRANSLATE_NOOP("PluginInfo", "AY8910/12 chip emulator and vtx file player"),
        QT_TRANSLATE_NOOP("PluginSettings", "Bits per sample (8 or 16)"),
    ]

    property var wavpack: [
        QT_TRANSLATE_NOOP("PluginInfo", "WavPack (.wv, .iso.wv) player"),
    ]

    property var wildmidi: [
        QT_TRANSLATE_NOOP("PluginInfo", "MIDI player based on WildMidi library\n\nRequires freepats package to be installed\nSee http://freepats.zenvoid.org/\nMake sure to set correct freepats.cfg path in plugin settings."),
        QT_TRANSLATE_NOOP("PluginSettings", "Timidity++ bank configuration file"),
    ]

    property var wma: [
        QT_TRANSLATE_NOOP("PluginInfo", "plays WMA files"),
    ]
}
