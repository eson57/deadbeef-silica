import QtQuick 2.0
import Sailfish.Silica 1.0

Item {
    property alias label: textField.label
    property alias path: textField.text

    anchors.left: parent.left
    anchors.right: parent.right

    height: textField.height

    TextField {
        id: textField

        anchors.left: parent.left
        anchors.right: selectionButton.left

        placeholderText: label

        EnterKey.iconSource: "image://theme/icon-m-enter-close"
        EnterKey.onClicked: focus = false
    }

    Button {
        id: selectionButton

        anchors.right: parent.right
        anchors.rightMargin: Theme.horizontalPageMargin

        width: height

        text: "..."

        onClicked: {
            // FIXME: Support passing current path to file dialog
            var dialog = pageStack.push(Qt.resolvedUrl("../dialogs/FilePicker.qml"),
                                        {
                                            title: "Select \"" + label + "\""
                                        });

            dialog.accepted.connect(function () {
                textField.text = dialog.resultPaths[0];
            });
        }
    }
}
