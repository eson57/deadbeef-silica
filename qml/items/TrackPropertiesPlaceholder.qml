import QtQuick 2.0
import Sailfish.Silica 1.0

Item {
    id: root

    property string text: "<Placeholder text>"

    anchors {
        left: parent.left
        right: parent.right
    }

    height: enabled ? placeholderLabel.height : 0
    visible: enabled

    Label {
        id: placeholderLabel

        anchors {
            left: parent.left
            leftMargin: Theme.horizontalPageMargin
            right: parent.right
            rightMargin: Theme.horizontalPageMargin
        }

        text: root.text
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        wrapMode: Text.Wrap
        color: Theme.secondaryHighlightColor
        font.pixelSize: Theme.fontSizeLarge
        font.family: Theme.fontFamilyHeading
    }
}
