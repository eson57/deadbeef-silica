/****************************************************************************************
**
** Copyright (C) 2013 Jolla Ltd.
** Contact: Martin Jones <martin.jones@jollamobile.com>
** All rights reserved.
**
** Copyright (C) 2018 Evgeny Kravchenko <cravchik@yandex.ru>
**
** This file is part of Sailfish Silica UI component package.
**
** You may use this file under the terms of BSD license as follows:
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Jolla Ltd nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
** ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR
** ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************************/

import QtQuick 2.4
import Sailfish.Silica 1.0

// FIXME: What is DragFilter from Sailfish.Silica.private and why is it needed for Slider?

MouseArea {
    id: slider

    property real maximumValue: 1.0
    property real minimumValue: 0.0
    property real stepSize
    property real value: 0.0
    readonly property real sliderValue: Math.max(minimumValue, Math.min(maximumValue, value))
    property bool handleVisible: true
    property string valueText
    property alias label: labelText.text
    property bool down: pressed && !_cancel
    property bool highlighted: down
    property real topMargin: Theme.paddingLarge * (labelText.visible ? 3 : 2)   // FIXME: This is not very robust
    property real bottomMargin: Theme.paddingLarge * 2

    property bool _hasValueLabel: false
    property Item _valueLabel
    property real _oldValue
    property real _precFactor: 1.0

    property real _grooveHeight: Math.max(0, height - topMargin - bottomMargin)
    property bool _heightChanged
    property bool _cancel

    property bool _componentComplete
    property int _extraPadding: Math.max(width - implicitWidth, 0) / 2

    onStepSizeChanged: {
        // Avoid rounding errors.  We assume that the range will
        // be sensibly related to stepSize
        var decimial = Math.floor(stepSize) - stepSize
        if (decimial < 0.001) {
            _precFactor = Math.pow(10, 7)
        } else if (decimial < 0.1) {
            _precFactor = Math.pow(10, 4)
        } else {
            _precFactor = 1.0
        }
    }

    // FIXME: Consider labelText width here
    implicitWidth: background.leftPadding + background.width + background.rightPadding

    implicitHeight: Theme.itemSizeExtraLarge * 3

    onHeightChanged: updateHeight()
    onTopMarginChanged: updateHeight()
    onBottomMarginChanged: updateHeight()

    // changing the height of the slider shouldn't animate the slider bar/handle
    function updateHeight() {
        _heightChanged = true
        _grooveHeight = Math.max(0, height - topMargin - bottomMargin)
        _heightChanged = false
    }

    function cancel() {
        _cancel = true
        value = _oldValue
    }

    drag {
        target: draggable
        minimumY: topMargin - highlight.height / 2
        maximumY: slider.height - bottomMargin - highlight.height / 2
        axis: Drag.YAxis
    }

    function _updateValueToDraggable() {
        if (height > (topMargin + bottomMargin)) {
            var pos = draggable.y + highlight.height / 2 - topMargin
            value = _calcValue(((_grooveHeight - pos) / _grooveHeight) * (maximumValue - minimumValue) + minimumValue)
        }
    }

    function _calcValue(newVal) {
        if (newVal <= minimumValue) {
            return minimumValue
        }

        if (stepSize > 0.0) {
            var offset = newVal - minimumValue
            var intervals = Math.round(offset / stepSize)
            newVal = Math.round((minimumValue + (intervals * stepSize)) * _precFactor) / _precFactor
        }

        if (newVal > maximumValue) {
            return maximumValue
        }

        return newVal
    }

    onPressed: {
        _cancel = false
        _oldValue = value
        draggable.y = Math.min(Math.max(drag.minimumY, mouseY - highlight.height / 2), drag.maximumY)
    }

    onReleased: {
        if (!_cancel) {
            _updateValueToDraggable()
            _oldValue = value
        }
    }

    onCanceled: {
        value = _oldValue
    }

    Component {
        id: valueIndicatorComponent

        Text {
            color: slider.highlighted ? Theme.highlightColor : Theme.primaryColor
            font.pixelSize: Theme.fontSizeHuge

            transformOrigin: Item.Right
            scale: slider.pressed ? 1.0 : (Theme.fontSizeLarge / Theme.fontSizeHuge)

            Behavior on scale { NumberAnimation { duration: 80 } }

            text: slider.valueText

            wrapMode: Text.Wrap
            width: Theme.itemSizeSmall

            horizontalAlignment: Qt.AlignRight

            anchors.verticalCenter: highlight.verticalCenter
            anchors.right: background.horizontalCenter
            anchors.rightMargin: Theme.paddingMedium + Theme.paddingSmall
            visible: text !== ""
        }
    }

    onValueTextChanged: {
        if (valueText && !_hasValueLabel) {
            _hasValueLabel = true
            _valueLabel = valueIndicatorComponent.createObject(slider)
        }
    }

    FontMetrics {
        id: fontMetrics
        font.pixelSize: Theme.fontSizeHuge
    }

    GlassItem {
        id: background
        // extra painting margins (Theme.paddingMedium on both sides) are needed,
        // because glass item doesn't visibly paint across the full width of the item
        property int rightPadding: (Theme.itemSizeSmall - width) / 2
        // ascent enough to keep text in slider area
        property int leftPadding: (slider._valueLabel != null && slider._valueLabel.visible) ? slider._valueLabel.width
                                                                                            : rightPadding

        y: slider.topMargin - Theme.paddingMedium
        height: slider._grooveHeight + 2 * Theme.paddingMedium
        // FIXME: Decide how to align background when label width is more than
        //        background width with both paddings
        x: slider._extraPadding + leftPadding
        width: Theme.itemSizeExtraSmall / 2

        dimmed: true
        radius: 0.06
        falloffRadius: 0.09
        ratio: 0.0
        color: slider.highlighted ? Theme.highlightColor : Theme.secondaryColor
    }

    GlassItem {
        id: progressBar
        anchors.bottom: background.bottom
        anchors.horizontalCenter: background.horizontalCenter
        // height added as GlassItem will not display correctly with width < height
        height: (sliderValue - minimumValue) / (maximumValue - minimumValue) * (background.height - width) + width
        width: Theme.itemSizeExtraSmall / 2
        visible: sliderValue > minimumValue
        dimmed: false
        radius: 0.05
        falloffRadius: 0.14
        ratio: 0.0
        color: slider.highlighted ? Theme.highlightColor : Theme.primaryColor
        Behavior on height {
            enabled: !_heightChanged
            SmoothedAnimation {
                duration: 300
                velocity: 1500*Theme.pixelRatio
            }
        }
    }

    Item {
        id: draggable
        height: highlight.height
        width: highlight.height
        onYChanged: {
            if (_cancel) {
                return
            }
            if (slider.drag.active) {
                _updateValueToDraggable()
            }
        }
    }
    GlassItem {
        id: highlight
        y: (maximumValue > minimumValue)
                ? _grooveHeight - (sliderValue - minimumValue) / (maximumValue - minimumValue) * _grooveHeight - highlight.height / 2 + topMargin
                : topMargin - highlight.height / 2
        width: Theme.itemSizeMedium
        height: Theme.itemSizeMedium
        radius: 0.17
        falloffRadius: 0.17
        anchors.horizontalCenter: background.horizontalCenter
        visible: handleVisible
        color: slider.highlighted ? Theme.highlightColor : Theme.primaryColor
        Behavior on y {
            enabled: !_heightChanged
            SmoothedAnimation {
                duration: 300
                velocity: 1500*Theme.pixelRatio
            }
        }
    }

    Label {
        id: labelText
        visible: text.length
        font.pixelSize: Theme.fontSizeSmall
        color: slider.highlighted ? Theme.secondaryHighlightColor : Theme.secondaryColor
        anchors.horizontalCenter: background.horizontalCenter
        anchors.top: parent.top
        truncationMode: TruncationMode.Fade
    }
    states: State {
        name: "invalidRange"
        when: _componentComplete && minimumValue >= maximumValue
        PropertyChanges {
            target: progressBar
            height: progressBar.width
        }
        PropertyChanges {
            target: slider
            enabled: false
            opacity: 0.6
        }
        StateChangeScript {
            script: console.log("Warning: Slider.maximumValue needs to be higher than Slider.minimumValue")
        }
    }

    Component.onCompleted: {
        _componentComplete = true
    }
}
