import QtQuick 2.0
import Sailfish.Silica 1.0

import deadbeef 1.0

import "../items"

Page {
    id: mainPage

    onStatusChanged: function() {
        if (status == PageStatus.Active) {
            var playlistPage = pageStack.pushAttached(Qt.resolvedUrl("PlaylistsPage.qml"));
            playlistPage.mainPage = mainPage;
        }
    }

    // FIXME: Why it can't be put inside PullDownMenu below?
    function runMediaFileDialog(onAccepted, title) {
        var lastOpenedDirectory = DdbApi.confGetStr("filechooser.lastdir", "");

        var dialog = pageStack.push(Qt.resolvedUrl("../dialogs/FilePicker.qml"),
                                                   {
                                                       path: lastOpenedDirectory,
                                                       selectDirectories: true,
                                                       multiple: true,
                                                       title: arguments.length >= 2 ? title : ""
                                                   });

        var supportedFormatsFilter = function(filename) {
            return DdbApi.isFormatSupported(filename);
        };

        var allFilesFilter = function(filename) {
            return true;
        };

        dialog.installFilter(qsTr("Supported formats"), supportedFormatsFilter);
        dialog.installFilter(qsTr("All files"), allFilesFilter);

        dialog.accepted.connect(function(){
            onAccepted(dialog.resultPaths);
        });

        dialog.done.connect(function(){
            DdbApi.confSetStr("filechooser.lastdir", dialog.path);
        });
    }

    function runLoadPlaylistFileDialog(onAccepted, title) {
        var lastOpenedDirectory = DdbApi.confGetStr("filechooser.playlist.lastdir", "");

        var dialog = pageStack.push(Qt.resolvedUrl("../dialogs/FilePicker.qml"),
                                                   {
                                                       path: lastOpenedDirectory,
                                                       title: arguments.length >= 2 ? title : ""
                                                   });

        function getExtension(filename) {
            var elements = filename.split(".");

            if (elements.length === 1 || (elements[0] === "" && elements.length === 2))
            {
                return "";
            }

            return elements.pop();
        }

        var supportedPlaylistFormatsFilter = function(filename) {
            var ext = getExtension(filename);
            return DdbApi.getSupportedPlaylistExtensions().indexOf(ext) !== -1;
        };

        var allFilesFilter = function() {
            return true;
        };

        dialog.installFilter(qsTr("Supported playlist formats"), supportedPlaylistFormatsFilter);
        dialog.installFilter(qsTr("All files"), allFilesFilter);

        dialog.accepted.connect(function() {
            onAccepted(dialog.resultPaths[0]);
        });

        dialog.done.connect(function() {
            DdbApi.confSetStr("filechooser.playlist.lastdir", dialog.path);
        });
    }

    SilicaFlickable {
        anchors.fill: parent
        contentHeight: height

        PullDownMenu {
            MenuItem {
                text: qsTr("Open file(s)/folder(s)", "item of pull-up menu")
                onClicked: runMediaFileDialog(function(selectedPaths){
                    DdbApi.openPaths(selectedPaths);
                },
                qsTr("Open file(s)/folder(s)", "title of dialog page"))
            }

            MenuItem {
                text: qsTr("Load playlist", "item of pull-up menu")
                onClicked: runLoadPlaylistFileDialog(function(selectedPath) {
                    DdbApi.loadPlaylist(selectedPath);
                },
                qsTr("Load playlist", "title of dialog page"))
            }

            MenuItem {
                text: qsTr("More...")
                onClicked: pageStack.push(Qt.resolvedUrl("../dialogs/PlaylistActions.qml"))
            }
        }

        PageHeader {
            id: playlistHeader
            anchors.top: parent.top
            title: DdbApi.playlistsModel.currentPlaylistTitle
        }

        SilicaListView {
            id: playlistView

            anchors.top: playlistHeader.bottom
            anchors.bottom: controlsPanel.top
            anchors.left: parent.left
            anchors.right: parent.right

            clip: true

            ViewPlaceholder {
                enabled: playlistView.count == 0
                text: qsTr("Empty playlist")
                hintText: qsTr("Pull down to add files")
                verticalOffset: height / 2 - controlsPanel.height
            }

            currentIndex: DdbApi.playitemsModel.nowPlayingIdx

            model: DdbApi.playitemsModel

            delegate: ListItem {
                id: listItem
                width: parent.width

                Column {
                    width: parent.width

                    Item {
                        width: parent.width
                        height: timeLabel.height

                        Label {
                            anchors.left: parent.left
                            anchors.leftMargin: Theme.horizontalPageMargin
                            anchors.right: timeLabel.left
                            anchors.rightMargin: Theme.paddingMedium
                            text: firstLine
                            truncationMode: TruncationMode.Elide
                            color: listItem.highlighted || isNowPlaying ? Theme.highlightColor :
                                                                          Theme.primaryColor
                        }

                        Label {
                            id: timeLabel
                            anchors.right: parent.right
                            anchors.rightMargin: Theme.horizontalPageMargin
                            text: duration
                            color: listItem.highlighted || isNowPlaying ? Theme.highlightColor :
                                                                          Theme.primaryColor
                        }
                    }

                    Label {
                        anchors.left: parent.left
                        anchors.leftMargin: Theme.horizontalPageMargin
                        anchors.right: parent.right
                        anchors.rightMargin: Theme.horizontalPageMargin
                        text: secondLine
                        font.pixelSize: Theme.fontSizeExtraSmall
                        truncationMode: TruncationMode.Elide
                        color: listItem.highlighted || isNowPlaying ? Theme.secondaryHighlightColor :
                                                                      Theme.secondaryColor
                    }
                }

                onClicked: DdbApi.playItemIdx(index)

                menu: contextMenuComponent

                Component {
                    id: contextMenuComponent

                    ContextMenu {
                        MenuItem {
                            text: qsTr("Add To Playback Queue")
                            onClicked: DdbApi.addToPlayqueue(index)
                        }

                        MenuItem {
                            text: qsTr("Remove From Playback Queue")
                            onClicked: DdbApi.removeFromPlayqueue(index)

                            enabled: isInPlayqueue
                            visible: enabled
                        }

                        MenuItem {
                            text: qsTr("Track Properties")
                            onClicked: pageStack.push(Qt.resolvedUrl("../dialogs/TrackProperties.qml"),
                                                      { trackIdx: index })
                        }

                        MenuItem {
                            text: qsTr("Delete")
                            onClicked: remove()
                        }
                    }
                }

                function remove() {
                    remorseAction(qsTr("Deleting"), function() { DdbApi.deleteItemIdx(index); });
                }
            }

            VerticalScrollDecorator {}
        }

        Controls {
            id: controlsPanel

            anchors.bottom: parent.bottom

            state: DdbApi.playbackState === DdbApi.PlaybackStopped ? "stopped" :
                   DdbApi.playbackState === DdbApi.PlaybackPaused  ? "paused" :
                                                                     "playing"

            playbackPosition: DdbApi.playbackPositionMs
            playbackDuration: DdbApi.playbackDurationMs

            onPrevClicked: DdbApi.prev()
            onPlayPauseClicked: DdbApi.playPause()
            onNextClicked: DdbApi.next()

            onSeekPerformed: DdbApi.setPlaybackPosition(position)
        }

        PushUpMenu {
            MenuItem {
                text: qsTr("Equalizer")
                onClicked: pageStack.push(Qt.resolvedUrl("../dialogs/Equalizer.qml"))

                visible: DdbApi.equalizerModel.available
            }

            MenuItem {
                text: qsTr("Settings")
                onClicked: pageStack.push(Qt.resolvedUrl("../dialogs/Settings.qml"))
            }

            MenuItem {
                text: qsTr("About")
                onClicked: pageStack.push(Qt.resolvedUrl("../dialogs/About.qml"))
            }
        }
    }
}
