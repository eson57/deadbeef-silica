<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>About</name>
    <message>
        <location filename="../qml/dialogs/About.qml" line="27"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="42"/>
        <source>DeadBeef-Silica GUI plugin %1
                                 Copyright © %2 Evgeny Kravchenko &lt;cravchik@yandex.ru&gt;
                                 This program is licensed under the terms of GPLv3 license
                                 
                                 Please use https://bitbucket.org/kravich/deadbeef-silica/issues                                  tracker to file bug reports and feature requests.
                                 
                                 Credits:
                                 
                                 %3</source>
        <translation>Графический интерфейс DeadBeef-Silica %1
                                 © %2 Евгений Кравченко &lt;cravchik@yandex.ru&gt;
                                 Программа распространяется на условиях лицензии GPLv3.
                                 
                                 Используйте https://bitbucket.org/kravich/deadbeef-silica/issues                                  чтобы сообщить об ошибке или предложить усовершенствование.
                                 
                                 Благодарности:
                                 
                                 %3</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="53"/>
        <source>Vyacheslav Dikonov &lt;sdiconov@mail.ru&gt;
                                                    Russian translation</source>
        <translation>Вячеслав Диконов &lt;sdiconov@mail.ru&gt;
                                                    Перевод на русский язык</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="94"/>
        <source>DeadBeef License</source>
        <translation>Лицензия DeadBeef</translation>
    </message>
</context>
<context>
    <name>AddLocation</name>
    <message>
        <location filename="../qml/dialogs/AddLocation.qml" line="21"/>
        <source>Add location</source>
        <translation>Добавление ссылки</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/AddLocation.qml" line="41"/>
        <source>Location (URI or file path)</source>
        <translation>Адрес (URI или путь к файлу)</translation>
    </message>
</context>
<context>
    <name>Equalizer</name>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="17"/>
        <source>Zero all</source>
        <translation>Сбросить всё</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="44"/>
        <source>Equalizer</source>
        <translation>Эквалайзер</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="56"/>
        <source>Enable</source>
        <translation>Включить</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="106"/>
        <source>Preamp</source>
        <translation>Усиление</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="134"/>
        <location filename="../qml/dialogs/Equalizer.qml" line="309"/>
        <source>+%1 dB</source>
        <translation>+%1 дБ</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="134"/>
        <location filename="../qml/dialogs/Equalizer.qml" line="309"/>
        <source>%1 dB</source>
        <translation>%1 дБ</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="192"/>
        <source>55 Hz</source>
        <translation>55 Гц</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="196"/>
        <source>77 Hz</source>
        <translation>77 Гц</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="200"/>
        <source>110 Hz</source>
        <translation>110 Гц</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="204"/>
        <source>156 Hz</source>
        <translation>156 Гц</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="208"/>
        <source>220 Hz</source>
        <translation>220 Гц</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="212"/>
        <source>311 Hz</source>
        <translation>311 Гц</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="216"/>
        <source>440 Hz</source>
        <translation>440 Гц</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="220"/>
        <source>622 Hz</source>
        <translation>622 Гц</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="224"/>
        <source>880 Hz</source>
        <translation>880 Гц</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="228"/>
        <source>1.2 kHz</source>
        <translation>1,2 кГц</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="232"/>
        <source>1.8 kHz</source>
        <translation>1,8 кГц</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="236"/>
        <source>2.5 kHz</source>
        <translation>2,5 кГц</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="240"/>
        <source>3.5 kHz</source>
        <translation>3,5 кГц</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="244"/>
        <source>5 kHz</source>
        <translation>5 кГц</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="248"/>
        <source>7 kHz</source>
        <translation>7 кГц</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="252"/>
        <source>10 kHz</source>
        <translation>10 кГц</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="256"/>
        <source>14 kHz</source>
        <translation>14 кГц</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="260"/>
        <source>20 kHz</source>
        <translation>20 кГц</translation>
    </message>
</context>
<context>
    <name>FilePicker</name>
    <message>
        <location filename="../qml/dialogs/FilePicker.qml" line="55"/>
        <source>Hide system files</source>
        <translation>Скрывать системные файлы</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FilePicker.qml" line="56"/>
        <source>Show system files</source>
        <translation>Показывать системные файлы</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FilePicker.qml" line="65"/>
        <source>Select current dir</source>
        <translation>Выбрать весь каталог</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FilePicker.qml" line="66"/>
        <source>Select</source>
        <translation>Выбрать</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FilePicker.qml" line="84"/>
        <source>Filter</source>
        <translation>Фильтр</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FilePicker.qml" line="108"/>
        <source>Path</source>
        <translation>Путь</translation>
    </message>
</context>
<context>
    <name>FileSaveDialog</name>
    <message>
        <location filename="../qml/dialogs/FileSaveDialog.qml" line="106"/>
        <source>Location</source>
        <translation>Каталог</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FileSaveDialog.qml" line="130"/>
        <source>Select file location</source>
        <translation>Выбор каталога</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FileSaveDialog.qml" line="145"/>
        <source>New file name</source>
        <translation>Имя нового файла</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FileSaveDialog.qml" line="163"/>
        <source>File exists. Overwrite?</source>
        <translation>Файл существует. Перезаписать?</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FileSaveDialog.qml" line="178"/>
        <source>There is already a directory with such name</source>
        <translation>Нельзя создать файл, так как уже есть каталог с таким именем</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FileSaveDialog.qml" line="194"/>
        <source>Format</source>
        <translation>Формат</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="38"/>
        <source>Supported formats</source>
        <translation>Известные форматы</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="39"/>
        <location filename="../qml/pages/MainPage.qml" line="80"/>
        <source>All files</source>
        <translation>Все файлы</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="79"/>
        <source>Supported playlist formats</source>
        <translation>Файлы списков</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="97"/>
        <source>Open file(s)/folder(s)</source>
        <comment>item of pull-up menu</comment>
        <translation>Открыть файл/каталог</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="101"/>
        <source>Open file(s)/folder(s)</source>
        <comment>title of dialog page</comment>
        <translation>Открытие файла/каталога</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="105"/>
        <source>Load playlist</source>
        <comment>item of pull-up menu</comment>
        <translation>Загрузить плейлист</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="109"/>
        <source>Load playlist</source>
        <comment>title of dialog page</comment>
        <translation>Загрузка плейлиста</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="113"/>
        <source>More...</source>
        <translation>Ещё...</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="136"/>
        <source>Empty playlist</source>
        <translation>Плейлист пуст</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="137"/>
        <source>Pull down to add files</source>
        <translation>Чтобы добавить файлы, потяните вниз</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="199"/>
        <source>Add To Playback Queue</source>
        <translation>Добавить в очередь проигрывания</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="204"/>
        <source>Remove From Playback Queue</source>
        <translation>Удалить из очереди</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="212"/>
        <source>Track Properties</source>
        <translation>Свойства дорожки</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="218"/>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="225"/>
        <source>Deleting</source>
        <translation>Удаление</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="253"/>
        <source>Equalizer</source>
        <translation>Эквалайзер</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="260"/>
        <source>Settings</source>
        <translation>Настройка</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="265"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
</context>
<context>
    <name>PlaylistActions</name>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="30"/>
        <source>Supported formats</source>
        <translation>Известные форматы</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="31"/>
        <source>All files</source>
        <translation>Все файлы</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="70"/>
        <source>Playlist actions</source>
        <translation>Редактор плейлиста</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="77"/>
        <source>Add file(s)/folder(s)</source>
        <comment>playlist actions menu item</comment>
        <translation>Добавить файлы</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="79"/>
        <source>Save playlist</source>
        <comment>playlist actions menu item</comment>
        <translation>Сохранить плейлист</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="90"/>
        <source>Add file(s)/folder(s)</source>
        <comment>file add dialog title</comment>
        <translation>Добавление файлов</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="108"/>
        <source>Save playlist</source>
        <comment>save playlist dialog title</comment>
        <translation>Сохранение плейлиста</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="78"/>
        <source>Add location</source>
        <translation>Добавить ссылку</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="80"/>
        <source>Sort playlist</source>
        <translation>Сортировать плейлист</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="81"/>
        <source>Clear playlist</source>
        <translation>Очистить плейлист</translation>
    </message>
</context>
<context>
    <name>PlaylistNameEdit</name>
    <message>
        <location filename="../qml/dialogs/PlaylistNameEdit.qml" line="22"/>
        <source>Add playlist</source>
        <translation>Добавить плейлист</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistNameEdit.qml" line="22"/>
        <source>Rename playlist</source>
        <translation>Переименовать плейлист</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistNameEdit.qml" line="42"/>
        <source>Playlist name</source>
        <translation>Название плейлиста</translation>
    </message>
</context>
<context>
    <name>PlaylistSorting</name>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="16"/>
        <source>Sort by</source>
        <translation>Способ сортировки</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="29"/>
        <source>Reverse order</source>
        <translation>В обратном порядке</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="45"/>
        <source>Artist/Album/Track number</source>
        <translation>Исполнитель/Альбом/Дорожка</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="46"/>
        <source>Artist/Album</source>
        <translation>Исполнитель/Альбом</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="47"/>
        <source>Title</source>
        <translation>Название</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="48"/>
        <source>Track number</source>
        <translation>Номер дорожки</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="49"/>
        <source>Album</source>
        <translation>Альбом</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="50"/>
        <source>Artist</source>
        <translation>Исполнитель</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="51"/>
        <source>Album artist</source>
        <translation>Автор альбома</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="52"/>
        <source>Date</source>
        <translation>Дата</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="53"/>
        <source>Filename</source>
        <translation>Имя файла</translation>
    </message>
</context>
<context>
    <name>PlaylistsPage</name>
    <message>
        <location filename="../qml/pages/PlaylistsPage.qml" line="40"/>
        <source>New playlist</source>
        <translation>Новый плейлист</translation>
    </message>
    <message>
        <location filename="../qml/pages/PlaylistsPage.qml" line="19"/>
        <source>Add new playlist</source>
        <translation>Добавить новый плейлист</translation>
    </message>
    <message>
        <location filename="../qml/pages/PlaylistsPage.qml" line="67"/>
        <source>Playlists</source>
        <translation>Плейлисты</translation>
    </message>
    <message>
        <location filename="../qml/pages/PlaylistsPage.qml" line="94"/>
        <source>Rename</source>
        <translation>Переименовать</translation>
    </message>
    <message>
        <location filename="../qml/pages/PlaylistsPage.qml" line="114"/>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="../qml/pages/PlaylistsPage.qml" line="121"/>
        <source>Deleting</source>
        <translation>Удаление</translation>
    </message>
</context>
<context>
    <name>PluginCopyright</name>
    <message>
        <location filename="../qml/dialogs/PluginCopyright.qml" line="24"/>
        <source>Copyright</source>
        <translation>Авторские права</translation>
    </message>
</context>
<context>
    <name>PluginInfo</name>
    <message>
        <location filename="../qml/dialogs/PluginInfo.qml" line="29"/>
        <source>Description</source>
        <translation>Описание</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PluginInfo.qml" line="53"/>
        <source>Version</source>
        <translation>Версия</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PluginInfo.qml" line="59"/>
        <source>Website</source>
        <translation>Веб-страница</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PluginInfo.qml" line="82"/>
        <source>Settings</source>
        <translation>Настройка</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PluginInfo.qml" line="90"/>
        <source>Copyright</source>
        <translation>Авторские права</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="31"/>
        <source>plays aac files, supports raw aac files, as well as mp4 container</source>
        <translation>Воспроизводит файлы AAC, поддерживает как просто aac-файлы, так и контейнер mp4.</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="35"/>
        <source>Adplug player (ADLIB OPL2/OPL3 emulator)</source>
        <translation>Проигрыватель Adplug (эмулятор ADLIB OPL2/OPL3)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="41"/>
        <source>plays alac files from MP4 and M4A files</source>
        <translation>Воспроизводит формат ALAC из файлов MP4 и M4A.</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="45"/>
        <source>plays psf, psf2, spu, ssf, dsf, qsf file formats</source>
        <translation>Воспроизводит файлы формата psf, psf2, spu, ssf, dsf, qsf.</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="49"/>
        <source>plays dts-encoded files using libdca from VLC project</source>
        <translation>Воспроизводит файлы в формате DTS с помощью библиотеки libdca из проекта VLC.</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="53"/>
        <source>User interface implemented using Sailfish Silica QML module</source>
        <translation>Графический интерфейс на основе Sailfish Silica QML.</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="57"/>
        <source>High quality samplerate converter using libsamplerate, http://www.mega-nerd.com/SRC/</source>
        <translation>Высококачественный преобразователь частоты семплирования с помощью libsamplerate, http://www.mega-nerd.com/SRC/</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="61"/>
        <source>module player based on DUMB library</source>
        <translation>Проигрыватель трекерных модулей основанный на библиотеке DUMB.</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="69"/>
        <source>APE player based on code from libavc and rockbox</source>
        <translation>Проигрыватель файлов формата APE основанный на коде проектов libavc и rockbox.</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="73"/>
        <source>decodes audio formats using FFMPEG libavcodec</source>
        <translation>Декодирует аудиоформаты с помощью FFMPEG libavcodec.</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="79"/>
        <source>FLAC decoder using libFLAC</source>
        <translation>Декодер FLAC использующий библиотеку libFLAC.</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="83"/>
        <source>chiptune/game music player based on GME library</source>
        <translation>Проигрыватель музыки для игр/звукосинтезирующих микросхем основанный на библиотеке GME.</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="90"/>
        <source>Sends played songs information to your last.fm account, or other service that use AudioScrobbler protocol</source>
        <translation>Передаёт информацию о музыке, которую вы слушаете, на last.fm или другие сервисы использующие протокол AudioScrobbler.</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="102"/>
        <source>Importing and exporting M3U and PLS formats
Recognizes .pls, .m3u and .m3u8 file types

NOTE: only utf8 file names are currently supported</source>
        <translation>Чтение и запись списков M3U и PLS
Поддерживает расширения файлов .pls, .m3u и .m3u8

ПРИМЕЧАНИЕ: для имён файлов сейчас поддерживается только кодировка utf8.</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="106"/>
        <source>MMS streaming plugin based on libmms</source>
        <translation>Поддержка протокола потокового вещания MMS на основе библиотеки libmms.</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="110"/>
        <source>Mono to stereo converter DSP</source>
        <translation>Преобразователь моно в стерео.</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="114"/>
        <source>MPEG v1/2 layer1/2/3 decoder

Using libmpg123 backend.
</source>
        <translation>Декодер MPEG1/2 layer1/2/3

Использует библиотеку libmpg123.
</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="120"/>
        <source>Communicate with other applications using D-Bus.</source>
        <translation>Взаимодействие с другими приложениями посредством D-Bus.</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="124"/>
        <source>Musepack decoder using libmppdec</source>
        <translation>Декодер Musepack использующий библиотеку libmppdec.</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="128"/>
        <source>This plugin takes the audio data, and discards it,
so nothing will play.
This is useful for testing.</source>
        <translation>Принимает звуковые данные и выбрасывает их.
Полезно для тестирования.</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="132"/>
        <source>At the moment of this writing, PulseAudio seems to be very unstable in many (or most) GNU/Linux distributions.
If you experience problems - please try switching to ALSA or OSS output.
If that doesn&apos;t help - please uninstall PulseAudio from your system, and try ALSA or OSS again.
Thanks for understanding</source>
        <translation>В настоящее время PulseAudio нестабильно во многих (или почти всех) дистрибутивах GNU/Linux.
Если у вас возникнут проблемы, попробуйте переключиться на ALSA или OSS.
Если не поможет, удалите PulseAudio из своей системы и попробуйте ALSA или OSS ещё раз.
Спасибо за понимание.</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="138"/>
        <source>SC68 player (Atari ST SNDH YM2149)</source>
        <translation>Проигрыватель SC68 (Atari ST SNDH YM2149)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="145"/>
        <source>decodes shn files</source>
        <translation>Декодирует файлы формата shn.</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="152"/>
        <source>SID player based on libsidplay2</source>
        <translation>Проирыватель SID на основе библиотеки libsidplay2</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="162"/>
        <source>wav/aiff player using libsndfile</source>
        <translation>Проигрыватель wav/aiff на основе библиотеки libsndfile.</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="167"/>
        <source>equalizer plugin using SuperEQ library</source>
        <translation>Эквалайзер на основе библиотеки SuperEQ.</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="171"/>
        <source>tta decoder based on TTA Hardware Players Library Version 1.2</source>
        <translation>Декодер формата tta на основе TTA Hardware Players Library версии 1.2.</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="175"/>
        <source>http and ftp streaming module using libcurl, with ICY protocol support</source>
        <translation>Поддержка потокового вещания по http и ftp с использованием libcurl и протокола ICY.</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="180"/>
        <source>Standard IO plugin
Used for reading normal local files
It is statically linked, so you can&apos;t delete it.</source>
        <translation>Стандартное расширение для ввода-вывода необходимое для чтения файлов с локального диска.
Линкуется статически, так что удалить его невозможно.</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="184"/>
        <source>play files directly from zip files</source>
        <translation>Воспроизведение файлов напрямую из zip-архивов.</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="188"/>
        <source>OggVorbis decoder using standard xiph.org libraries</source>
        <translation>Декодер OggVorbis использующий стандартные библиотеки xiph.org.</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="192"/>
        <source>AY8910/12 chip emulator and vtx file player</source>
        <translation>Эмулятор микросхемы AY8910/12 и проигрыватель файлов формата vtx.</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="197"/>
        <source>WavPack (.wv, .iso.wv) player</source>
        <translation>Проигрыватель WavPack (.wv, .iso.wv).</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="201"/>
        <source>MIDI player based on WildMidi library

Requires freepats package to be installed
See http://freepats.zenvoid.org/
Make sure to set correct freepats.cfg path in plugin settings.</source>
        <translation>Проирыватель MIDI-музыки на основе библиотеки WildMidi

Требует установки пакета сэмплов freepats
См. http://freepats.zenvoid.org/
Убедитесь, что в параметрах расширения верно указан путь к файлу freepats.cfg.</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="206"/>
        <source>plays WMA files</source>
        <translation>Воспроизводит файлы формата WMA (Windows Media Audio).</translation>
    </message>
</context>
<context>
    <name>PluginSettings</name>
    <message>
        <location filename="../qml/dialogs/PluginSettings.qml" line="15"/>
        <source>%1 settings</source>
        <translation>Настройка 
%1</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="91"/>
        <source>Enable scrobbler</source>
        <translation>Включить скробблер</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="92"/>
        <source>Disable nowplaying</source>
        <translation>Не сообщать, что вы слушаете</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="93"/>
        <source>Username</source>
        <translation>Имя пользователя</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="94"/>
        <source>Password</source>
        <translation>Пароль</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="95"/>
        <source>Scrobble URL</source>
        <translation>URL скробблера</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="96"/>
        <source>Prefer Album Artist over Artist field</source>
        <translation>Предпочитать тег автора альбома вместо исполнителя</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="97"/>
        <source>Send MusicBrainz ID</source>
        <translation>Отправлять идентификатор MusicBrainz</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="98"/>
        <source>Submit tracks shorter than 30 seconds (not recommended)</source>
        <translation>Отправлять данные о дорожках короче 30 сек. (не рекомендуется)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="62"/>
        <source>Resampling quality (0..5, higher is better)</source>
        <translation>Качество ресемплинга 
(0..5, больше - лучше)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="63"/>
        <source>8-bit output (default is 16)</source>
        <translation>8-битный вывод 
(по-умолчанию 16)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="64"/>
        <source>Internal DUMB volume (0..128)</source>
        <translation>Внутренняя регулировка громкости 
(0..128)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="65"/>
        <source>Volume ramping (0 is none, 1 is note on/off, 2 is always)</source>
        <translation>Подавление щелчков 
(0 отключено, 1 одна нота, 2 постоянно)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="84"/>
        <source>Max song length (in minutes)</source>
        <translation>Макс. длина дорожки</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="85"/>
        <source>Fadeout length (seconds)</source>
        <translation>Длительность затухания в конце (сек.)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="86"/>
        <source>Play loops nr. of times (if available)</source>
        <translation>Повторять циклы N раз (если возможно)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="133"/>
        <source>PulseAudio server</source>
        <translation>Сервер PulseAudio</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="134"/>
        <source>Preferred buffer size</source>
        <translation>Размер буфера</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="146"/>
        <source>Relative seek table path</source>
        <translation>Относительный путь таблицы поиска</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="147"/>
        <source>Absolute seek table path</source>
        <translation>Абсолютный путь таблицы поиска</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="148"/>
        <source>Swap audio bytes (toggle if all you hear is static)</source>
        <translation>Сменить порядок байтов (если вы слышите шум вместо музыки)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="153"/>
        <source>Enable HVSC Songlength DB</source>
        <translation>Использовать БД HVSC Songlength</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="154"/>
        <source>Songlengths.txt (from HVSC)</source>
        <translation>Songlengths.txt (от HVSC)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="140"/>
        <location filename="../qml/translation/ExtraStrings.qml" line="155"/>
        <source>Samplerate</source>
        <translation>Частота семплирования</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="156"/>
        <location filename="../qml/translation/ExtraStrings.qml" line="193"/>
        <source>Bits per sample (8 or 16)</source>
        <translation>Битность (8 или 16)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="157"/>
        <source>Mono synth</source>
        <translation>Моно синтезатор</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="158"/>
        <source>Default song length (sec)</source>
        <translation>Стандартная длина дорожки (сек.)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="202"/>
        <source>Timidity++ bank configuration file</source>
        <translation>Файл конфигурации инструментов Timidity++</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="74"/>
        <source>Use all extensions supported by ffmpeg</source>
        <translation>Использовать все расширения поддерживаемые ffmpeg</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="75"/>
        <location filename="../qml/translation/ExtraStrings.qml" line="163"/>
        <source>File Extensions (separate with &apos;;&apos;)</source>
        <translation>Расширения имён файлов 
(разделитель &apos;;&apos;)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="115"/>
        <source>Disable gapless playback (faster scanning)</source>
        <translation>Отключить режим воспроизведения без пауз (ускоряет прокрутку)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="116"/>
        <source>Backend</source>
        <translation>Бэкенд</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="36"/>
        <source>Prefer Ken emu over Satoh (surround won&apos;t work)</source>
        <translation>Предпочитать Ken emu вместо Satoh (многоканальность не поддерживается)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="37"/>
        <source>Enable surround</source>
        <translation>Включить многоканальный режим</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="139"/>
        <source>Default song length (in minutes)</source>
        <translation>Стандартная длина дорожки (в минутах)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="141"/>
        <source>Skip when shorter than (sec)</source>
        <translation>Пропускать дорожки короче чем (сек.)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="176"/>
        <source>Emulate track change events (for scrobbling)</source>
        <translation>Симулировать смену дорожек (для скробблера)</translation>
    </message>
</context>
<context>
    <name>Plugins</name>
    <message>
        <location filename="../qml/dialogs/Plugins.qml" line="11"/>
        <source>Plugins</source>
        <translation>Расширения</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="18"/>
        <source>Settings</source>
        <translation>Настройка</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="22"/>
        <source>Playback options</source>
        <translation>Параметры воспроизведения</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="26"/>
        <source>Looping</source>
        <translation>Повторение</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="35"/>
        <source>Don&apos;t loop</source>
        <translation>Не зацикливать</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="41"/>
        <source>Loop single song</source>
        <translation>Зациклить одну песню</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="47"/>
        <source>Loop all</source>
        <translation>Зациклить всё</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="54"/>
        <source>Playback order</source>
        <translation>Порядок</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="64"/>
        <source>Linear</source>
        <translation>По очереди</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="70"/>
        <source>Shuffle tracks</source>
        <translation>Перемешивать дорожки</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="76"/>
        <source>Shuffle albums</source>
        <translation>Перемешивать альбомы</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="82"/>
        <source>Random</source>
        <translation>Случайно</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="116"/>
        <source>Setup Last.fm</source>
        <translation>Настройка Last.fm</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="126"/>
        <source>Plugins</source>
        <translation>Расширения</translation>
    </message>
</context>
<context>
    <name>TrackProperties</name>
    <message>
        <location filename="../qml/dialogs/TrackProperties.qml" line="28"/>
        <source>Track Properties</source>
        <translation>Свойства дорожки</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/TrackProperties.qml" line="30"/>
        <source>Metadata</source>
        <translation>Метаданные</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/TrackProperties.qml" line="34"/>
        <source>No metadata</source>
        <translation>Нет метаданных</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/TrackProperties.qml" line="52"/>
        <source>Properties</source>
        <translation>Свойства</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/TrackProperties.qml" line="56"/>
        <source>No properties</source>
        <translation>Нет свойств</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="8"/>
        <source>Artist</source>
        <translation>Исполнитель</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="9"/>
        <source>Track Title</source>
        <translation>Название</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="10"/>
        <source>Album</source>
        <translation>Альбом</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="11"/>
        <source>Date</source>
        <translation>Дата</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="12"/>
        <source>Track Number</source>
        <translation>Номер дорожки</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="13"/>
        <source>Total Tracks</source>
        <translation>Всего дорожек</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="14"/>
        <source>Genre</source>
        <translation>Жанр</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="15"/>
        <source>Composer</source>
        <translation>Композитор</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="16"/>
        <source>Disc Number</source>
        <translation>Номер диска</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="17"/>
        <source>Total Disks</source>
        <translation>Всего дисков</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="18"/>
        <source>Comment</source>
        <translation>Примечания</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="22"/>
        <source>Location</source>
        <translation>Ссылка</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="23"/>
        <source>Subtrack Index</source>
        <translation>Индекс вложенных дорожек</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="24"/>
        <source>Duration</source>
        <translation>Длительность</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="25"/>
        <source>Tag Type(s)</source>
        <translation>Типы тегов</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="26"/>
        <source>Embedded Cuesheet</source>
        <translation>Встроенный cue</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="27"/>
        <source>Codec</source>
        <translation>Кодек</translation>
    </message>
</context>
</TS>
