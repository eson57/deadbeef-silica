TEMPLATE = lib

TARGET = ddb_gui_silica

CONFIG += plugin no_plugin_name_prefix link_pkgconfig

QT += quick qml
PKGCONFIG += audioresource-qt

# Limit API version
DEFINES += "DDB_API_LEVEL=8"

SOURCES += \
    src/ddb_gui_silica.cpp \
    src/ddbapi.cpp \
    src/filesystemmodel.cpp \
    src/trackmetadatamodel.cpp \
    src/configdialogmodel.cpp \
    src/strutils.cpp \
    src/configschemeparser.cpp \
    src/fs.cpp \
    src/logger.cpp \
    src/abstractqmllistmodel.cpp

# Specify files to parse by lupdate
lupdate_only {
    SOURCES += qml/cover/*.qml \
        qml/dialogs/*.qml \
        qml/items/*.qml \
        qml/pages/*.qml \
        qml/translation/*.qml \
        qml/*.qml
}

RESOURCES += deadbeef-silica.qrc

HEADERS += \
    src/ddbapi.h \
    src/filesystemmodel.h \
    src/trackmetadatamodel.h \
    src/configdialogmodel.h \
    src/strutils.h \
    src/configschemeparser.h \
    src/fs.h \
    src/logger.h \
    src/version.h \
    src/abstractqmllistmodel.h

# This is needed by QtCreator to find deadbeef/deadbeef.h file for autocompletion
INCLUDEPATH += /usr/include/

target.path = /usr/lib/deadbeef

desktop.files = deadbeef-silica.desktop
desktop.path = /usr/share/applications

INSTALLS += target desktop

ICON_SIZES = 86x86 108x108 128x128 256x256

for(size, ICON_SIZES) {
    icon$${size}.files = icons/$${size}/deadbeef-silica.png
    icon$${size}.path = /usr/share/icons/hicolor/$${size}/apps
    INSTALLS += icon$${size}
}

TRANSLATIONS += \
    translations/deadbeef-silica-ru.ts

OTHER_FILES += \
    qml/translation/ExtraStrings.qml \
    rpm/deadbeef-silica.yaml \
    rpm/deadbeef-silica.spec \
    deadbeef-silica.desktop
